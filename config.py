from dataclasses import dataclass, field
from enum import Enum
from pathlib import Path
from datetime import datetime
import hashlib
# import os

from infos import NAME, VERSION, AUTHOR
from webapp import models
import webapp.utils.file_io as fio

# basedir = os.path.abspath(os.path.dirname(__file__))
# SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(basedir, 'app.db')



class AppEnv(Enum):
    DEV = 0
    PROD = 1



@dataclass
class AppConfig:
    ROOT: "Path | None" = None
    SETTINGS: "Path | None" = None
    LANGUAGES: "Path | None" = None
    THEMES: "Path | None" = None

    MODS: "Path | None" = None
    OUTPUT: "Path | None" = None
    OVERWRITE: "Path | None" = None

    CONFIG: dict = field(default_factory=dict)

    KEY: "str | None" = None

    URL: tuple[str, ...] = field(default_factory=tuple)

    def __post_init__(self):
        self.ROOT = Path(__file__).parent                           # TODO Add overwrite to parsers (ini & plugins), especially if fast_parser ON
        self.SETTINGS = self.ROOT / "settings" / "settings.json"
        self.LANGUAGES = self.ROOT / "settings" / "i18n"
        self.THEMES = self.ROOT / "settings" / "themes"

        self.URL = ("https://www.dochash.fr/", "skyrim/records/SPIDER/",  "skyrim/keys/gen/", "skyrim/keys/check/")
        # self.URL = ("http://localhost:9000/", "tools/records/") # DEBUG

        try: self.load_config()
        except Exception as error:                              # TODO Handle errors
            self.CONFIG = {                                     # Use default config
                "path": { "mods": "", "output": "", "custom": [] },           # TODO +/- (re)create settings.json file and dirs (if not exists) and save default config inside
                "exports": { "DISTR": { "prefix": "SPIDER", "format": "" } },
                "imports": { "DISTR": [] },
                "share_records": True,
                "key": "",
                "explorer": { "fast_parser": True, "alpha_order": True }
                }
            print(f"ERROR: {error}")
        
        # Path checker for both output & mods path
        self.resolve_path(name='mods', default_value=self.ROOT.parent.parent)       # NOTE  Default values correspond to the valid path for MO2 users
        self.resolve_path(name='output', default_value=self.ROOT.parent)            #       (eg. MO2_ROOT = self.ROOT.parent.parent)
        # TODO Do the same for OVERWRITE (add it to streamline the __post_init__ path checkers and setters)

        self.OVERWRITE = self.MODS.parent / "overwrite"                             # NOTE GET MO2_OVERWRITE PATH WHEN INSTALLED CORRECTLY
        if not self.OVERWRITE.exists():
            self.OVERWRITE = None

        self.KEY = self.CONFIG.get('key')

        if self.CONFIG['share_records']:
            try: self._check_key()
            except PermissionError as permission_denied:
                print("--------------permission denied")
                self.CONFIG['share_records'] = False
            except Exception as error:
                print("-------------except")
                try: self._gen_key()
                except: self.CONFIG['share_records'] = False
    
    def _gen_key(self):
        # timestamp = datetime.now().strftime('%Y-%m-%d|%H:%M:%S')
        # data = { "name": NAME, "version": VERSION, "author": AUTHOR, "timestamp": timestamp }
        data = { "name": NAME, "version": VERSION, "author": AUTHOR }
        data.update(self.CONFIG)

        req = models.ExportedData(
            data=data,
            path=self.SETTINGS,
            url=f"{self.URL[0]}{self.URL[2]}"
            )
        
        print(req)                                  # DEBUG - TO REMOVE

        if not req.time:
            raise ValueError(f'Invalid value: error during key request initialization (timestamp={req.time})')
        if not req.data:
            raise ValueError(f'Invalid value: error during key request initialization (data={req.data})')
        if not isinstance(req.data, dict):
            raise TypeError(f'Invalid type: error during key request initialization (datatype={type(req.data)})')
        
        print("------ REQUESTING A NEW KEY... ------")  # DEBUG - TO REMOVE

        timestamp = req.time.strftime('%Y-%m-%d|%H:%M:%S')                  # Same format as serveur
        p = ''.join([c for c in str(req.path) if str(req.path).index(c) % 2])
        h_data = f"<$s{req.data['name']}--v{req.data['version']}$d{timestamp}$s{req.data['author']}$s{p}>"
        h = hashlib.sha256(h_data.encode()).hexdigest()

        req.data.update({ 'timestamp': timestamp, 'p': p, 'uid': h })
        print(req)                                  # DEBUG - TO REMOVE
        
        # TODO Send a request to the server to set self.KEY
        response = req.export(local=False)                              # TODO add return response into models.ExportedData

        # TODO
        if not response:
            raise ConnectionError()
        
        # DEBUG - TO REMOVE
        # TODO Get key from response, then add key into self.CONFIG and save_config()
        # TODO After (in try/except statement?), do a _get_key() to set self.PASS
        print(" ---------- RESPONSE 1 ----------")
        print(response)
        print(response.__dict__)          # TODO errors to fix/comment
        print(response.json())            # TODO errors to fix/comment

        self.KEY = response.json().get('key')
        self.CONFIG['key'] = self.KEY if self.KEY else ""
        self.save_config(data=self.CONFIG)

    def _check_key(self):

        if not self.KEY:
            print("--------------------no key")
            raise ValueError('Invalid value: user key could not be found.')

        print("---------- CHECKING KEY... ----------")  # DEBUG - TO REMOVE

        req = models.ExportedData(
            data={ "key": self.KEY },
            url=f"{self.URL[0]}{self.URL[3]}"
            )
        
        print(req)                                      # DEBUG - TO REMOVE
        
        response = req.export(local=False)

        # TODO On Server Side:
        #       1. Check key files into server data
        #       2. If a file with the key string exists, load it
        #       3. Get the timestamp and is_valid values from the given key file
        #       4. Make a screenshot of datetime.now() at the request time
        #       5. Save the request time screenshot into the key file (last cnx)
        #       6. Check if is_valid value is True
        #               --> If True, return { auth: True }
        #               --> Else, return { auth: False }
        #       NB: wrap it all into a specific function called by the check route into view,
        #           to be able to perform the same check from any other requested route on
        #           server side, but to return various response/data
        # TODO On Client Side:
        #       1. Check if response.json() has been authentified ('is_auth' = True)
        #               --> If not, raise a Permission Error
        #       2. Return response.json() from _check_key() (with received data from server)

        # TODO
        if not response:
            raise ConnectionError()
        
        if not response.json().get('allowed'):                      # is_auth
            print("----------------------pas  is_auth")
            raise PermissionError('Permission denied: user key is either invalid or could not be checked for now.')

        # DEBUG - TO REMOVE
        print(" ---------- RESPONSE 2 ----------")
        print(response)
        print(response.json())            # TODO errors to fix/comment

        return response.json()
    
    def _get_valid_path(self, key: str) -> "Path|None":
        try:
            value = self.CONFIG.get('path', {}).get(key)
            if not value:
                raise Exception(f'No custom path was given for {key} directory')
            if not isinstance(value, str):
                raise TypeError(f'Invalid type: {value} is not a string')
            if not Path(value).exists():
                raise ValueError(f'Invalid value: {value} is not a valid path')                     # TODO Also check if is a valid dir with is_dir ?
            return Path(value)
        except:
            return None

    def resolve_path(self, name: str, default_value: "Path|None") -> None:
        if not hasattr(self, name.upper()):
            raise AttributeError(f'Invalid value: {name} is not a valid config attribute')
        checked_path = self._get_valid_path(key=name.lower())
        setattr(self, name.upper(), checked_path if checked_path else default_value)

    def load_config(self) -> dict:
        if not isinstance(self.SETTINGS, Path) or not self.SETTINGS.exists():
            raise ValueError('Invalid value: settings.json file path is not valid')
        
        config = fio.load_data(file=self.SETTINGS)
        self.CONFIG = config if isinstance(config, dict) else {}
        
        return self.CONFIG
    
    def save_config(self, data: dict) -> None:                      # TODO Rework with no data arg, replaced by self.CONFIG ? (same as load_config())
        if not isinstance(self.SETTINGS, Path) or not self.SETTINGS.exists():
            raise ValueError('Invalid value: settings.json file path is not valid')
        
        fio.save_data(data=data, file=self.SETTINGS)
        self.load_config()



APP = AppConfig()
INPUT = models.AppInput()
EXPLORER = models.AppExplorer()
USER_RECORDS = models.UserRecord()
USER_DISTR = models.Records()
LOGS = models.Logger(
    active=models.Log(),
    level=models.LogCategory['Debug']                       # Info OR Error
)



# RECORD_TYPES = [
#     "Spell",
#     "Perk",
#     "Item",             # special format
#     "Shout",
#     "LevSpell",
#     "Package",          # special format
#     "Outfit",
#     "Keyword",
#     "DeathItem",
#     "Faction",
#     "SleepOutfit",
#     "Skin"
# ]

USER_SELECTION = {}

USER_REQUESTS = []
# USER_RECORDS = {
#     'id': "",
#     'traits': "",
#     'variable': "test",
#     'chance': "100"
# }
# USER_RECORDS = models.UserRecord(
#     record_type=models.RecordType['Spell'],
#     id=models.RecordId(),
#     strings=models.StringFilters(),
#     forms=models.FormFilters(),
#     lvl=models.LevelFilters(),
#     traits=models.Traits(),
#     options=models.RecordOptions()
#     ) # TO COMPLETE
USER_ACTION = ""


# USER_DISTR = {
#     'uncategorized': []
# }
# # # # # # # # # # FOR TESTS # # # # # # # # # #
TEST = {}
RECTEST = {}
# # # # # # # # # # /FOR TESTS # # # # # # # # # #
