# Virtual environment (python3.10.7)
# python3.10 -m venv .env
# source .env/bin/activate

NAME = "spider-app"
VERSION = "1.0.0"
AUTHOR = "Aewin aka DaimyoTaiShi"

# SPID templates
# RecordType = RecordID|StringFilters|FormFilters|LevelFilters|Traits|NONE|Chance
# Item = RecordID|StringFilters|FormFilters|LevelFilters|Traits|ItemCount|Chance
# Package = RecordID|StringFilters|FormFilters|LevelFilters|Traits|PackageIdx|Chance


# Use Flask to make desktop app / exe
# https://medium.com/@fareedkhandev/create-desktop-application-using-flask-framework-ee4386a583e9




#region TODO ATTENTION NEEDED & TO FIX
# # ##### Parsing does not work fine in these cases:
# TODO Check why these mistakes and fix them
# # ;  [[CATEGORY: Test7 ]]
# # ;  [[    CATEGORY: Test5 ]]

# # ##### Others to check:
# Penser à gérer les doublons de Gender M/F dans Traits
# Trouver pourquoi un mauvais RecordId renvoie NONE comme FormFilters (idem pour StringFilters ? en tous cas idem pour StringFilters vide ou avec caractères useless (-, ))
# setattr(self, attr, (pattern, trait[value]))
# Dans UserRecord :
# [DONE]    setattr(self, attr, value if value and is_string_valid else ATTRS[attr]())
# [DONE]    Check que les return soient liés à une string == "NONE" pour ne pas les flagger comme "invalid"
#endregion


#region TESTS
# # test = {'message': "hello world!"}
# # test = """Item = testid|-hello+nope,hi|0xAEZ~Skyrim.esm|2(10/-20)|F/-U/W/S|-2|190   """
# # test = """Keyword = 0xAE~test.esp|NONE|NONE|NONE|NONE|NONE|80"""
# # test = """Package = testid|-hello+nope,hi|0xAE~Skyrim.esm|2(10/20)|F/-U/S|3|90"""
# # test = """Package = testid|-hello+nope,hi|0xAZE~Skyrim.esm|2(-10/20)|F/-UU/S|101|190"""
# # test = """Package = 0xAE~test.esp||0xAE~Sky~rim.esm|2(-10/20)|F/-U/S|-1|10"""
# test = """Spell = 0x123~Skyrim.esm   

# Keyword = 0xAE~test.esp|NONE|NONE|NONE|NONE|NONE|80
# ;;  coucou 

# Item = testid|-hello+nope,hi|0xAE~Skyrim.esm|2(10/20)|F/-U/S|2|90
# ;test

# ;[[ CATEGORY:      TeSt   ]]
# Perk=0xED~Skyrim.esm

# ;[[ CATEGORY:     unCateGorized  ]]
# Shout=0x456~uncat.esp
# """

# # rectest = "Item = 0x123Z~Sk~yrim.esm|-hello+nope,hi|0xAE~Skyrim.esm|10/20-|F/-U/S|2|90"
# # rectest = "Item = 0x123~Skyrim.esm|-hello+nope,hi|0xAE~Skyrim.esm|1(10/20)|F/-U/S|2|90"
# # rectest = "Item = 0x123~Skyrim.esm|-hello+nope,hi|0xAE~Skyrim.esm|1(10/20)|-M|2|90"
# # rectest = "Item = 0x123~Skyrim.esm|-hello+- +-+nope+ ++coucou+,hi,, ,-, -  ,yop|0xAE~Skyrim.esm|1(10/20)|-M|2|90"
# # rectest = "Item = 0x123~Skyrim.esm|-  +- +-++ ++   +, ---,, ,-, -  ,-  |0xAE~Skyrim.esm|1(10/20)|-M/W/U|2|90"
# # rectest = "Item = 0x123~Skyrim.esm|-hello,-+ |0xAE~Skyrim.esm|1(10/20)|-M/W/U|2|90"
# # rectest = "Spell = 0x123~Skyrim.esm|NONE|-0x23~Skyrim.esm+0xAE~test.esp+*test,hello,-0xF3~Skyrim.esm|NONE|NONE|NONE|80"
# # rectest = "Spell = 0x123~Skyrim.esm|NONE|-, ,,+, -|NONE|NONE|NONE|80"
# rectest = "Spell = 0x123~Skyrim.esm|NONE|-*|NONE|NONE|NONE|80"
# # rectest = "Item = 0x123~Sk~yrim.esm|-hello+nope,hi|0xAE~Skyrim.esm|10/20-|F/-U/S|2|90"
# # rectest = "Item = testid|-hello+nope,hi|0xAE~Skyrim.esm|10/20-|F/-U/S|2|90"
# # rectest = "Item = testid|-hello+nope,hi|0xAE~Skyrim.esm|10/20|F/-U/S|2|90"
# # rectest = "Item = testid|-hello+nope,hi|0xAE~Skyrim.esm|10|F/-U/S|2|90"
# # rectest = "Item = testid|-hello+nope,hi|0xAE~Skyrim.esm|2(10)|F/-U/S|2|90"
# # rectest = "Item = testid|-hello+nope,hi|0xAE~Skyrim.esm|2(10/20)|F/-U/S|2|90"
# # rectest = "Spell = testid|-hello+nope,hi|0xAE~Skyrim.esm|2(10/20)|F/-U/S|NONE|90"
# # rectest = "Spell = 0x123Z~Skyrim.esm   "
# # rectest = "Spell = 0x123~Skyrim.esm   "
#endregion
