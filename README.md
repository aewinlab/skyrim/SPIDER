# SPIDER - Spell Perk Item Distributor Easy Resource



## What is SPIDER?

SPIDER (Spell Perk Item Distributor Easy Resource) is a utility program designed to centralize, check, sort and manage all the contents related to [Spell Perk Item Distributor (SPID)](https://www.nexusmods.com/skyrimspecialedition/mods/36869).

It allows for easy organization and control of user records distributions, all in one place, with a user-friendly UI and powered by useful features.

## Getting started

1. [Download](https://www.nexusmods.com/skyrimspecialedition/mods/98177) the mod from the Nexus, and install it with your favorite mod manager.
2. Run the executable. A page should open automatically in your default browser.
3. If nothing happens, you can manually open your favorite browser and enter the URL [http://localhost:5000/](http://localhost:5000/).

## Current tools

- [ ] **Nav menu (top)**: switch between the different Record types used by SPID.
- [ ] **Widgets (center top)**: create, edit, check and optimize a Record to distribute through SPID. Submit it to add it to your current distribution.
- [ ] **Distribution (center bottom)**: display all Records to distribute through SPID, sorted by categories. You can import records from existing distribution files, edit each record and category, and export/save your current distribution into a new file.
- [ ] **Settings menu (bottom left)**: edit your settings (eg., custom folder paths, record sharing...) and exit the app. Hover to expand.
- [ ] **Explorer menu (right)**: parse selected mods from your modlist, allowing to get records, filter them, display relevant info, and easily populate widgets. Hover to expand.

## Need more help?

Each widget includes a **`?` button (top right)** that can be clicked to access a video tutorial for helpful tips on its functionality.

Please note that some tutorials are still WIP.

The **full playlist** can be found [here](https://www.youtube.com/watch?v=VLd_c-8m91g&list=PLuWIjlx3PFSX_G7WL3qbHR2ZBIcfAtc53).

***

# Other useful info

Here will be added useful info such as roadmap, version tracking, acknowledgements, license. To complete.

## Current Version
1.0.0

## Description
```WIP.```

## Usage
```WIP.```

## Support
```WIP.```

## Roadmap
```WIP```

## Contributing
```WIP.```

## Authors and acknowledgment
```WIP.```

## License
```WIP.```

## Project status
```WIP.```
