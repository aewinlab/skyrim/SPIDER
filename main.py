# SPIDER - Spell Perk Item Distributor Easy Resource

import webbrowser
import ctypes

from webapp import app
from config import AppEnv



ENV = AppEnv['PROD']                                     # Switch to 'PROD' before making a Distributable
URL = "http://localhost:5000"                           # HTTPS does not work for localhost server


def hide_console():
    """Workaround to hide console in the distributable desktop app."""

    whnd = ctypes.windll.kernel32.GetConsoleWindow()
    if whnd != 0:
        ctypes.windll.user32.ShowWindow(whnd, 0)



if __name__ == "__main__":
    webbrowser.open(URL)
    
    # Prod Environment
    if ENV == AppEnv['PROD']:
        hide_console()
        app.run(debug=False)
    
    # Dev Environment
    else:
        app.run(debug=True)
