from pathlib import Path
import json



def load_data(file: Path) -> "dict | str | None":
    is_json = True if file.suffix == ".json" else False
    is_txt = True if file.suffix in (".txt", ".ini") else False

    if not any(i for i in (is_json, is_txt)): return

    with open(file, 'r', encoding='utf-8') as f:
        return json.load(f) if is_json else f.read()


def save_data(data: "dict | str", file: Path) -> None:
    if not isinstance(data, (dict, str)):
        raise TypeError("Invalid type: data should be either a string or a dictionnary")

    is_json = True if file.suffix == ".json" else False
    is_txt = True if file.suffix in (".txt", ".ini") else False

    if not (is_json or is_txt):
        raise TypeError('Invalid type: file extension should be either "txt", "ini" or "json"')

    with file.open('w', encoding='utf-8') as f:
        if is_json and type(data) == dict: json.dump(data, f, ensure_ascii=False, indent=4)
        elif is_txt and type(data) == str: f.write(data)
