

# rework docstring + comments
def clean_strings(input: list[str], chars: str = " +-") -> list:
    return [item.strip(chars) for item in input if item.strip(chars) != ""]


#rework docstring + comments
def form_to_tuple(string: str) -> tuple:
    data = [_.strip(" ") for _ in string.strip("()").split(',')]

    if len(data) < 1: return ()

    formatted_data = []

    for item in data:
        if item.isdigit(): formatted_data.append(int(item))
        elif item.isdecimal(): formatted_data.append(float(item))
        elif item.startswith("'") and item.endswith("'"): formatted_data.append(item.strip("'"))
        elif item.startswith('"') and item.endswith('"'): formatted_data.append(item.strip('"'))
        else: raise TypeError(f"Invalid type: {item}")
    
    return tuple(formatted_data)


#rework docstring + comments & add to core
def str_to_bool(string: str) -> bool:
    if string.lower() == "true":
        return True
    elif string.lower() == "false":
        return False
    else:
        raise ValueError(f'Invalid value: {string} is not a boolean')