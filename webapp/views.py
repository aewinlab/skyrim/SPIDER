from pathlib import Path
from datetime import datetime
import copy
import os

from flask import render_template, url_for, request, redirect, jsonify
import requests as r

import skyrim_tools.utils.skyrimtools as skyrim

from . import app
from .constants import TRAITS_INPUTS, TUTORIALS, IMPORTS_KEYS, APP_MODULES, RECORDTYPES_NAVICONS, MARKERS_COLORS
from .models import SubmitType, RecordType, RecordIdType, RecordId, StringFilter, StringFilters, FormFilter, FormFilters, LevelFilterType, Skills, FormId, Gender, Unique, Summonable, Child, LogCategory, Log, Logger, ExportedData, Records, AppInputTag
# from .utils import form_to_tuple, switch_trait_value, switch_trait_modifier, get_record_id, get_level_filters, set_active_filter, add_new_filter, remove_active_filter, add_string_to_filter, add_to_filter, remove_string_filter, update_string_filter_from_string, update_string_filters_from_string, get_string_filter, get_string_filters, get_form_filter, get_filter, get_filters
from .utils.dataformat import form_to_tuple, str_to_bool
from .utils.records import switch_trait_value, switch_trait_modifier, get_record_id, get_level_filters, set_active_filter, add_new_filter, remove_active_filter, add_to_filter, get_filter, get_filters



@app.route('/', methods=['GET', 'POST'])
@app.route('/index/', methods=['GET', 'POST'])
def index():

    #region Webapp Components and Values
    APP = app.config['APP']
    INPUT = app.config['INPUT']
    EXPLORER = app.config['EXPLORER']
    LOGGER = app.config['LOGS']
    DISTRIBUTION = app.config['USER_DISTR']
    #endregion

    if request.method == 'POST':

        #region Popup & Logger + Overlays initialization
        LOGGER.new()
        INPUT.reset()
        #endregion

        #region Nav Links
        if request.form.get('record_type'):
            record_type = request.form['record_type']
            app.config['USER_RECORDS'].record_type = RecordType[record_type]

            if EXPLORER.filter in RecordType.__members__:
                EXPLORER.set_filter(filter=record_type)
            # app.config['USER_SELECTION']['record_type'] = record_type
            # ADD SOMETHING TO RESET CARDS VALUES WHEN SWITCHING RECORD_TYPE ? PROBABLY NOT, MAYBE ONLY RESET VARIABLE FIELDS ? PROBABLEMENT LE PLUS SIMPLE SERAIT DE RESET ITEMCOUNT SAUF SI RECORD_TYPE = ITEM ET PACKAGEIDX SAUF SI RECORD_TYPE = PACKAGE
        #endregion

        #region Search Bar
        if request.form.get('user_input'):
            user_input = request.form['user_input']
            app.config['USER_REQUESTS'].append(user_input)
        #endregion

        #region Buttons
        elif "remove_item" in request.form:
            item = request.form.get('remove_item')
            while item in app.config['USER_REQUESTS']:
                app.config['USER_REQUESTS'].remove(item)

        elif "test" in request.form:
        # elif request.form.get('test'):
            app.config['USER_REQUESTS'].append('HELLO THERE!')
        #endregion

        #region Settings Menu
        elif 'settings_toggle_sharing_submit' in request.form:
            APP.CONFIG['share_records'] = not APP.CONFIG['share_records']
            APP.save_config(data=APP.CONFIG)
        
        elif 'settings_edit_dirpath_submit' in request.form:
            # Open an overlay with dirpath selectors and a submit button
            INPUT.tag = AppInputTag['IsSettingCustomPathActive']
            INPUT.value = {}

        elif 'settings_open_prompt_submit' in request.form:                     # TODO
            # Penser à changer l'icone pour une bulle de dialogue p ex
            # 1. Open an overlay by passing an INPUT tag and value
            # 2. The overlay enables an input text with +/- submit button and/or a table to display results
            # 3. When confirmed, the values must be passed to user_record
            pass

        elif 'custom_dirpath_submit' in request.form:
            mods_dirpath = request.form.get('custom_dirpath_mods_input', '')
            output_dirpath = request.form.get('custom_dirpath_output_input', '')

            # New custom dirpath are applied to APP.CONFIG and settings.json is updated with the new config
            APP.CONFIG['path']['mods'] = mods_dirpath
            APP.CONFIG['path']['output'] = output_dirpath
            APP.save_config(data=APP.CONFIG)
            
            # Check if the new dirpath are still valid and resolve them
            APP.resolve_path(name='mods', default_value=APP.ROOT.parent.parent)
            APP.resolve_path(name='output', default_value=APP.ROOT.parent)
            # TODO Add other new dirpath & settings
        #endregion

        #region Explorer Menu
        elif 'explorer_mods_reload' in request.form:
            EXPLORER.parse_folder(folder=APP.MODS)

        elif 'explorer_records_parse' in request.form:
            mods = [Path(i) for i in request.form if request.form.get(i)]
            EXPLORER.get_references(files=mods)

        elif 'explorer_records_filter_submit' in request.form:
            selected_filter = request.form.get('explorer_records_filter_submit')
            EXPLORER.set_filter(filter=selected_filter)
        
        elif 'explorer_record_fill_id_submit' in request.form:
            record_id_input = request.form.get('explorer_record_fill_id_submit')

            if record_id_input:
                record_id = RecordId()
                record_id.from_string(string=record_id_input)
                app.config['USER_RECORDS'].id = record_id
        #endregion

        #region RecordID
        elif 'id_save' in request.form:
            record_id = get_record_id()

            if app.config['USER_RECORDS'].id.id_type == RecordIdType['FormID']:
                app.config['USER_RECORDS'].id.form_id = record_id.get('id')
                app.config['USER_RECORDS'].id.mod = record_id.get('mod')

            elif app.config['USER_RECORDS'].id.id_type == RecordIdType['EditorID']:
                app.config['USER_RECORDS'].id.editor_id = record_id.get('id')
            
            #region Logger (debug)
            record_type = app.config['USER_RECORDS'].record_type
            id_type = app.config['USER_RECORDS'].id.id_type
            record_id = app.config['USER_RECORDS'].id

            LOG_NOT = " not" if False else ""
            LOG_RECORD = f" for the current {record_type.name} record" if record_type else ""
            LOG_IDTYPE = f" ({id_type.name} type)"
            LOG_VALUE = f" to {record_id._get_value()}"

            LOGGER.new(
                hdr="debug",
                msg=f"Record ID{LOG_IDTYPE}{LOG_RECORD} was{LOG_NOT} set{LOG_VALUE}")
            #endregion

        elif 'id_type_input' in request.form:
            id_type_input = request.form.get('id_type_input')
            id_type = app.config['USER_RECORDS'].id.id_type

            # Resets RecordID values when switching RecordIdType
            app.config['USER_RECORDS'].id.editor_id = ""
            app.config['USER_RECORDS'].id.form_id = FormId()
            app.config['USER_RECORDS'].id.mod = ""

            if id_type_input and id_type_input != id_type:
                app.config['USER_RECORDS'].id.id_type = RecordIdType[id_type_input]
        #endregion

        #region StringFilters
        elif 'string_filters_save' in request.form:

            if app.config['USER_RECORDS'].strings.active == 0:
                string_filters_input = request.form.get('string_filters_input')

                app.config['USER_RECORDS'].strings = get_filters(
                    string=string_filters_input if string_filters_input else "",
                    filter_type=StringFilter)

            else:
                active_index = app.config['USER_RECORDS'].strings.active
                string_filter_input = request.form.get('string_filter_input')
                
                app.config['USER_RECORDS'].strings.content[active_index - 1] = get_filter(
                    string=string_filter_input if string_filter_input else "",
                    filter_type=StringFilter
                    )

                app.config['USER_RECORDS'].strings.content[active_index - 1].index = active_index

                if len(app.config['USER_RECORDS'].strings.content[active_index - 1].strings) < 1:
                    remove_active_filter('strings')                 # remove_string_filter()

        elif 'string_filter_select_index' in request.form:
            set_active_filter(
                filters='strings',
                index=request.form.get('string_filter_select_index')
                )
        
        elif 'string_filter_add' in request.form:
            add_new_filter('strings', StringFilter)

        elif 'string_filter_exclude' in request.form:
            active_index = app.config['USER_RECORDS'].strings.active
            if active_index != 0:
                app.config['USER_RECORDS'].strings.content[active_index - 1].switch_pattern("-")

        elif 'string_filter_remove' in request.form:
            remove_active_filter('strings')
        
        elif 'add_string_to_filter' in request.form:
            string_input = request.form.get('string_input')         # OLD: # add_string_to_filter()

            add_to_filter(
                filters='strings',
                input=string_input if string_input else ""
            )
        #endregion

        #region FormFilters
        elif 'form_filters_save' in request.form:

            if app.config['USER_RECORDS'].forms.active == 0:
                form_filters_input = request.form.get('form_filters_input')

                app.config['USER_RECORDS'].forms = get_filters(
                    string=form_filters_input if form_filters_input else "",
                    filter_type=FormFilter)

            else:
                if app.config['USER_RECORDS'].forms.new_form:
                    app.config['USER_RECORDS'].forms.new_form = None

                else:
                    active_index = app.config['USER_RECORDS'].forms.active
                    form_filter_input = request.form.get('form_filter_input')

                    app.config['USER_RECORDS'].forms.content[active_index - 1] = get_filter(
                        string=form_filter_input if form_filter_input else "",
                        filter_type=FormFilter
                        )
                    
                    app.config['USER_RECORDS'].forms.content[active_index - 1].index = active_index

                    if len(app.config['USER_RECORDS'].forms.content[active_index - 1].forms) < 1:
                        remove_active_filter('forms')               # remove_string_filter()

        elif 'form_filter_select_index' in request.form:
            set_active_filter(
                filters='forms',
                index=request.form.get('form_filter_select_index')
                )
            app.config['USER_RECORDS'].forms.new_form = None
        
        elif 'form_type_input' in request.form:
            form_type_input = request.form.get('form_type_input')

            if form_type_input:
                app.config['USER_RECORDS'].forms.new_form = RecordId()
                app.config['USER_RECORDS'].forms.new_form.id_type = RecordIdType[form_type_input]
        
        elif 'back_to_filter' in request.form:
            app.config['USER_RECORDS'].forms.new_form = None
        
        elif 'form_item_include' in request.form:
            app.config['USER_RECORDS'].forms.new_form.switch_pattern("*")

        elif 'form_filter_add' in request.form:
            add_new_filter('forms', FormFilter)
        
        elif 'form_filter_exclude' in request.form:
            active_index = app.config['USER_RECORDS'].forms.active
            if active_index != 0:
                app.config['USER_RECORDS'].forms.content[active_index - 1].switch_pattern("-")
        
        elif 'form_filter_remove' in request.form:
            remove_active_filter('forms')
        
        elif 'add_form_to_filter' in request.form:
            record_id = get_record_id(app.config['USER_RECORDS'].forms.new_form)

            if app.config['USER_RECORDS'].forms.new_form.id_type == RecordIdType['FormID']:
                app.config['USER_RECORDS'].forms.new_form.form_id = record_id.get('id')
                app.config['USER_RECORDS'].forms.new_form.mod = record_id.get('mod')

            elif app.config['USER_RECORDS'].forms.new_form.id_type == RecordIdType['EditorID']:
                app.config['USER_RECORDS'].forms.new_form.editor_id = record_id.get('id')
            
            add_to_filter(
                filters='forms',
                input=app.config['USER_RECORDS'].forms.new_form
            )

            app.config['USER_RECORDS'].forms.new_form = None
        #endregion

        #region LevelFilters
        elif 'level_save' in request.form:
            
            level_filters = get_level_filters()
            app.config['USER_RECORDS'].lvl.skill = level_filters.get('skill')
            app.config['USER_RECORDS'].lvl.bounds = level_filters.get('bounds')

            #region Logger (debug)
            record_type = app.config['USER_RECORDS'].record_type
            filter_type = app.config['USER_RECORDS'].lvl.filter_type
            bounds = app.config['USER_RECORDS'].lvl.bounds
            skill = app.config['USER_RECORDS'].lvl.skill

            LOG_NOT = " not" if filter_type == LevelFilterType['NONE'] else ""
            LOG_RECORD = f" for the current {record_type.name} record" if record_type else ""
            LOG_FILTER = "" if filter_type == LevelFilterType['NONE'] else f" ({filter_type.name} type)"
            min = bounds[0] if bounds and bounds[0] else '0'
            max = bounds[1] if bounds and bounds[1] else '∞'
            LOG_BOUNDS = "" if filter_type == LevelFilterType['NONE'] else f" to [{min};{max}]"
            LOG_SKILL = "" if skill == Skills['NONE'] or skill == None else f" for {skill.name} skill"

            LOGGER.new(
                hdr="debug",
                msg=f"Level filter{LOG_FILTER}{LOG_RECORD} was{LOG_NOT} set{LOG_BOUNDS}{LOG_SKILL}")
            #endregion

        elif 'filter_type_input' in request.form:

            filter_type_input = request.form.get('filter_type_input')
            filter_type = app.config['USER_RECORDS'].lvl.filter_type

            if filter_type_input:
                if filter_type == LevelFilterType[filter_type_input]:
                    app.config['USER_RECORDS'].lvl.filter_type = LevelFilterType['NONE']
                
                else:
                    app.config['USER_RECORDS'].lvl.filter_type = LevelFilterType[filter_type_input]
            
            level_filters = get_level_filters()
            app.config['USER_RECORDS'].lvl.skill = level_filters.get('skill')
            app.config['USER_RECORDS'].lvl.bounds = level_filters.get('bounds')
        #endregion

        #region Traits
        #region OLD VERSION - TRAITS
        # elif 'gender_input' in request.form:
        #     app.config['USER_RECORDS'].traits.gender = switch_trait_value(Gender)

        #     #region Logger (debug)
        #     record_type = app.config['USER_RECORDS'].record_type
        #     value = app.config['USER_RECORDS'].traits.gender[1]
        #     trait = type(value)
        #     gender = "Male" if value == trait['M'] else "Female" if value == trait['F'] else ""
        #     pattern = app.config['USER_RECORDS'].traits.gender[0]

        #     LOG_NOT = " not" if value == trait['NONE'] else ""
        #     LOG_RECORD = f" for the current {record_type.name} record" if record_type else ""
        #     LOG_TRAIT = f" {trait.__name__}"
        #     action = " exclude" if pattern == "-" and value != trait['NONE'] else ""
        #     LOG_VALUE = "" if value == trait['NONE'] else f" to{action} {gender}"

        #     app.config['LOGS'].new(
        #         hdr="debug",
        #         msg=f"Trait{LOG_TRAIT}{LOG_RECORD} was{LOG_NOT} set{LOG_VALUE}")
        #     #endregion

        # elif 'unique_input' in request.form:
        #     app.config['USER_RECORDS'].traits.unique = switch_trait_value(Unique)

        #     #region Logger (debug)
        #     record_type = app.config['USER_RECORDS'].record_type
        #     value = app.config['USER_RECORDS'].traits.unique[1]
        #     trait = type(value)
        #     unique = "Unique" if value == trait['U'] else ""
        #     pattern = app.config['USER_RECORDS'].traits.unique[0]

        #     LOG_NOT = " not" if value == trait['NONE'] else ""
        #     LOG_RECORD = f" for the current {record_type.name} record" if record_type else ""
        #     LOG_TRAIT = f" {trait.__name__}"
        #     action = " exclude" if pattern == "-" and value != trait['NONE'] else ""
        #     LOG_VALUE = "" if value == trait['NONE'] else f" to{action} {unique}"

        #     app.config['LOGS'].new(
        #         hdr="debug",
        #         msg=f"Trait{LOG_TRAIT}{LOG_RECORD} was{LOG_NOT} set{LOG_VALUE}")
        #     #endregion

        # elif 'summonable_input' in request.form:
        #     app.config['USER_RECORDS'].traits.summonable = switch_trait_value(Summonable)

        #     #region Logger (debug)
        #     record_type = app.config['USER_RECORDS'].record_type
        #     value = app.config['USER_RECORDS'].traits.summonable[1]
        #     trait = type(value)
        #     summonable = "Summonable" if value == trait['S'] else ""
        #     pattern = app.config['USER_RECORDS'].traits.summonable[0]

        #     LOG_NOT = " not" if value == trait['NONE'] else ""
        #     LOG_RECORD = f" for the current {record_type.name} record" if record_type else ""
        #     LOG_TRAIT = f" {trait.__name__}"
        #     action = " exclude" if pattern == "-" and value != trait['NONE'] else ""
        #     LOG_VALUE = "" if value == trait['NONE'] else f" to{action} {summonable}"

        #     app.config['LOGS'].new(
        #         hdr="debug",
        #         msg=f"Trait{LOG_TRAIT}{LOG_RECORD} was{LOG_NOT} set{LOG_VALUE}")
        #     #endregion

        # elif 'child_input' in request.form:
        #     app.config['USER_RECORDS'].traits.child = switch_trait_value(Child)

        #     #region Logger (debug)
        #     record_type = app.config['USER_RECORDS'].record_type
        #     value = app.config['USER_RECORDS'].traits.child[1]
        #     trait = type(value)
        #     child = "Child" if value == trait['C'] else ""
        #     pattern = app.config['USER_RECORDS'].traits.child[0]

        #     LOG_NOT = " not" if value == trait['NONE'] else ""
        #     LOG_RECORD = f" for the current {record_type.name} record" if record_type else ""
        #     LOG_FILTER = f" ({trait.__name__} type)"
        #     action = ""
        #     if pattern == "-" and value != trait['NONE']: action = " exclude"
        #     elif not pattern and value != trait['NONE']: action = " include"
        #     # action = " exclude" if pattern == "-" and value != trait['NONE'] else ""
        #     LOG_VALUE = "" if value == trait['NONE'] else f" to{action} {child}"

        #     app.config['LOGS'].new(
        #         hdr="debug",
        #         msg=f"Traits filter{LOG_FILTER}{LOG_RECORD} was{LOG_NOT} set{LOG_VALUE}")
        #     #endregion

        # elif 'gender_mod_input' in request.form:
        #     app.config['USER_RECORDS'].traits.gender = switch_trait_modifier(Gender)

        # elif 'unique_mod_input' in request.form:
        #     app.config['USER_RECORDS'].traits.unique = switch_trait_modifier(Unique)

        # elif 'summonable_mod_input' in request.form:
        #     app.config['USER_RECORDS'].traits.summonable = switch_trait_modifier(Summonable)

        # elif 'child_mod_input' in request.form:
        #     app.config['USER_RECORDS'].traits.child = switch_trait_modifier(Child)
        #endregion
        elif any(i in request.form for i in TRAITS_INPUTS):
            TRAITS = {
                "gender": Gender,
                "unique": Unique,
                "summonable": Summonable,
                "child": Child
                }
            GENDERS = { Gender['M']: "Male", Gender['F']: "Female" }
            
            try:
                entrypoint = [i for i in TRAITS_INPUTS if i in request.form]

                if len(entrypoint) != 1:
                    raise ValueError("Invalid value: Trait entrypoint must contain exactly one valid input value")

                trait_input = entrypoint[0]

                if trait_input.endswith("_mod_input"):
                    trait = trait_input[:-10]
                    switch_function = switch_trait_modifier
                elif trait_input.endswith("_input"):
                    trait = trait_input[:-6]
                    switch_function = switch_trait_value
                else:
                    raise ValueError(f"Invalid value: Trait input value ({trait_input}) must be a valid trait name")

                cls = TRAITS.get(trait)

                if not cls:
                    raise ValueError(f"Invalid value: Trait name ({trait}) does not match with any valid trait class")

                setattr(
                    app.config['USER_RECORDS'].traits,
                    trait,
                    switch_function(cls)
                    )
                
                #region Logger (debug)
                record_type = app.config['USER_RECORDS'].record_type
                traits = getattr(app.config['USER_RECORDS'].traits, trait)
                pattern, value = traits

                LOG_NOT = " not" if value == cls['NONE'] else ""
                LOG_RECORD = f" for the current {record_type.name} record" if record_type else ""
                LOG_ACTION = "" if value == cls['NONE'] else f" to {'ex' if pattern == '-' else 'in'}clude"
                var = GENDERS.get(value) if cls == Gender and value != cls['NONE'] else ""
                LOG_VALUE = " for" if value == cls['NONE'] else f" {var}"
                LOG_FILTER = f" {cls.__name__}"

                LOGGER.new(
                    hdr="debug",
                    msg=f"Traits filter{LOG_RECORD} was{LOG_NOT} set{LOG_ACTION}{LOG_VALUE}{LOG_FILTER}")
                #endregion

            except Exception as error:
                #region Logger (error)
                LOGGER.new(
                    hdr="error",
                    msg=f"[TRAITS] {error}")
                #endregion
        #endregion

        #region VariableField
        elif 'variable_save' in request.form:
            count = request.form.get('item_count_input')
            package = request.form.get('package_idx_input')

            # Check if input values are not null and are valid numeric values
            app.config['USER_RECORDS'].item_count = int(count) if count and count.isdigit() and int(count) > 0 else 1
            app.config['USER_RECORDS'].package_idx = int(package) if package and package.isdigit() else 0

            #region Logger (debug)
            record_type = app.config['USER_RECORDS'].record_type
            item_count = app.config['USER_RECORDS'].item_count
            package_idx = app.config['USER_RECORDS'].package_idx

            if record_type == RecordType['Item']:
                LOG_TITLE = "Count"
                LOG_VALUE = f" was set to {item_count}"
            elif record_type == RecordType['Package']:
                LOG_TITLE = "Index"
                LOG_VALUE = f" was set to {package_idx}"
            else:
                LOG_TITLE = "Variable field"
                LOG_VALUE = ""
            LOG_NOT = "" if record_type in [RecordType['Item'], RecordType['Package']] else " is disabled"
            LOG_RECORD = f" for the current {record_type.name} record" if record_type else ""

            LOGGER.new(
                hdr="debug",
                msg=f"{LOG_TITLE}{LOG_NOT}{LOG_RECORD}{LOG_VALUE}")
            #endregion
        #endregion

        #region Chance
        elif 'chance_save' in request.form:
            chance = request.form.get('chance_input')

            # Check if input value is not null and is a valid numeric value
            app.config['USER_RECORDS'].chance = int(chance) if chance and chance.isdigit() else 100

            #region Logger (debug)
            record_type = app.config['USER_RECORDS'].record_type
            chance = app.config['USER_RECORDS'].chance

            LOG_NOT = " not" if False else ""
            LOG_RECORD = f" for the current {record_type.name} record" if record_type else ""
            LOG_VALUE = f" to {chance}%"

            LOGGER.new(
                hdr="debug",
                msg=f"Distribution chance{LOG_RECORD} was{LOG_NOT} set{LOG_VALUE}")
            #endregion
        #endregion

        #region Submit
        elif 'submit_save' in request.form:
            category_name_input = request.form.get('category_name_input')

            if not category_name_input: category_name_input = ""                            # = uncategorized
            category_name_input = category_name_input.strip(" ")
            if category_name_input.lower() == "uncategorized": category_name_input = ""     # = uncategorized
            
            app.config['USER_RECORDS'].options.category = category_name_input.lower()
            DISTRIBUTION._new_category(
                name=category_name_input.lower()
            )

            # app.config['USER_RECORDS'].options.category = ""

        elif 'submit_type_input' in request.form:
            submit_type_input = request.form.get('submit_type_input')
            submit_type = app.config['USER_RECORDS'].options.submit_type

            if submit_type_input:
                if submit_type == SubmitType[submit_type_input]:
                    app.config['USER_RECORDS'].options.submit_type = SubmitType['NONE']
                else:
                    app.config['USER_RECORDS'].options.submit_type = SubmitType[submit_type_input]
            
            app.config['USER_RECORDS'].options.category = ""
            
        elif 'add_record' in request.form:
            # ajouter idem submit_save avant ?
            # BEING TESTED... +/- A REWORK CAR IDEM submit_save ?
            category_name_input = request.form.get('category_name_input')

            if not category_name_input: category_name_input = ""
            category_name_input = category_name_input.strip(" ")
            if category_name_input.lower() == "uncategorized": category_name_input = ""

            app.config['USER_RECORDS'].options.category = category_name_input.lower()
            DISTRIBUTION._new_category(
                name=category_name_input.lower()
            )
            # /TESTS

            record_copy = copy.deepcopy(app.config['USER_RECORDS'])
            record_category = record_copy.options.category

            DISTRIBUTION._add_record(
                record=record_copy,
                category=record_category if record_category != "" else None
                )

        # elif 'add_category' in request.form:
        #     category_name = request.form.get('category_name_input')
        #     if not category_name: category_name = ""
        #     app.config['USER_DISTR']._new_category(
        #         name=category_name
        #     )
        #     print(app.config['USER_DISTR'])

        #endregion

        #region Distribution - Categories
        elif 'move_up_category' in request.form:
            selected_category = request.form.get('move_up_category')

            DISTRIBUTION._move_up_category(
                name=selected_category if selected_category else ""
            )

        elif 'move_down_category' in request.form:
            selected_category = request.form.get('move_down_category')

            DISTRIBUTION._move_down_category(
                name=selected_category if selected_category else ""
            )
        
        elif 'rename_category' in request.form:
            INPUT.tag = AppInputTag['IsRenamingCategoryActive']
            
            category = request.form.get('rename_category')
            if category == "uncategorized": category = ""
            # TODO Log Warning to handle "Uncategorized" (category="") case (cannot be renamed)
                
            INPUT.value = { "name": category.capitalize() if category else "" }
        
        elif 'rename_category_submit' in request.form:
            category = request.form.get('rename_category_submit')
            new_name = request.form.get('rename_category_input')

            # Handle "Uncategorized" (ie. category = "") cases
            if category and category.lower() == "uncategorized": category = ""
            if new_name and new_name.lower() == "uncategorized": new_name = ""

            DISTRIBUTION._rename_category(
                current_name=category.lower() if category else "",
                new_name=new_name.strip().lower() if new_name else ""
            )
        
        elif 'collapse_expand_category' in request.form:
            selected_category = request.form.get('collapse_expand_category')

            DISTRIBUTION._collapse_or_expand_category(
                name=selected_category if selected_category else ""
            )
        
        elif 'remove_category' in request.form:
            selected_category = request.form.get('remove_category')
            selected_category = selected_category.lower() if selected_category else ""

            if selected_category in DISTRIBUTION.categories:
                for record in DISTRIBUTION.__dict__[selected_category].content:
                    record_copy = copy.deepcopy(record)
                    DISTRIBUTION._add_record(record=record_copy)
                    
                DISTRIBUTION._del_category(name=selected_category)
        #endregion

        #region Distribution - Records
        elif 'move_up_record' in request.form:
            record = request.form.get('move_up_record')
            category, record_index = form_to_tuple(record) if record else (None, None)

            if category and record_index != None:
                DISTRIBUTION._move_up_record(
                    index=record_index,
                    category=category
                    )

        elif 'move_down_record' in request.form:
            record = request.form.get('move_down_record')
            category, record_index = form_to_tuple(record) if record else (None, None)

            if category and record_index != None:
                DISTRIBUTION._move_down_record(
                    index=record_index,
                    category=category
                    )
        
        elif 'change_record_category' in request.form:              # Ou 'move_record_to_category'
            INPUT.tag = AppInputTag['IsChangingCategoryActive']

            record = request.form.get('change_record_category')
            category, record_index = form_to_tuple(record) if record else (None, None)
            selected_record = None

            if category and record_index != None:
                selected_record = DISTRIBUTION._get_record(
                    index=record_index,                         # +/- if record_index else -1
                    category=category
                )

            INPUT.value = {
                "category": category,
                "record": selected_record._get_value() if selected_record else "",
                "submitted_values": (category, record_index)
            }
        
        elif 'change_category_submit' in request.form:
            record = request.form.get('change_category_submit')
            category, record_index = form_to_tuple(record) if record else (None, None)
            new_category = request.form.get('change_category_input')

            if category and new_category and record_index != None:
                DISTRIBUTION._change_record_category(
                    index=record_index,                         # +/- if record_index else -1
                    current_category=category,
                    new_category=new_category
                )

        elif 'get_record' in request.form:
            record = request.form.get('get_record')
            category, record_index = form_to_tuple(record) if record else (None, None)

            if category and record_index != None:
                record = DISTRIBUTION._get_record(
                    index=record_index,
                    category=category,
                    extract=True
                    )
                if record: app.config['USER_RECORDS'] = record

        elif 'remove_record' in request.form:
            record = request.form.get('remove_record')
            category, record_index = form_to_tuple(record) if record else (None, None)

            if category and record_index != None:
                DISTRIBUTION._remove_record(
                    index=record_index,
                    category=category
                    )
        #endregion

        #region Tutorials
        elif any(key in request.form for key in TUTORIALS.keys()):
            INPUT.tag = AppInputTag['IsTutorialActive']

            key = next(filter(lambda key: key in request.form, TUTORIALS), 'PLAYLIST')
            src = TUTORIALS.get(key)
            INPUT.value = { "src": src }
        #endregion

        #region Files manager
        elif 'import_distribution' in request.form:
            INPUT.tag = AppInputTag['IsImportActive']

            IMPORTS = APP.CONFIG['imports']['DISTR']
            
            files = skyrim.get_spid_files(APP.MODS)
            updated_imports = []
            # app.config['INPUT'].value = [{ "path": f, "selected": True } for f in files]

            for f in files:
                try:
                    item = next(filter(lambda i: i.get('path') == str(f), IMPORTS), None)

                    if item and set(item.keys()) == IMPORTS_KEYS:
                        updated_imports += [item]
                        continue

                    updated_imports += [{
                        "path": str(f),
                        "name": f"{f.name} (.../{f.parent.relative_to(APP.MODS)})",
                        "selected": True,
                        "disabled": False
                    }]

                except Exception as error:                              # TODO Handle errors
                    print(f"ERROR: {error}")
            
            APP.CONFIG['imports']['DISTR'] = INPUT.value = updated_imports
            APP.save_config(data=APP.CONFIG)
            
            # OLD VERSION
            # SETTINGS = skyrim.fio.load_data(file=app.config['APP'].SETTINGS)
            # if isinstance(SETTINGS, dict):
            #     SETTINGS['imports']['DISTR']['active'] = [{ "path": str(f), "selected": True } for f in files]
            #     skyrim.fio.save_data(data=SETTINGS, file=app.config['APP'].SETTINGS)
        
        elif 'imports_submit' in request.form:
            IMPORTS = APP.CONFIG['imports']['DISTR']
            failed_imports = set()

            # OPTIONAL CHECK
            # submitted = request.form.get('imports_submit')
            # length = int(submitted) if submitted and submitted.isdigit() else 0
            # if length != len(imports): raise ValueError()

            for i in IMPORTS:
                try:
                    f = i.get('path')
                    checked = request.form.get(f)
                    is_selected = str_to_bool(checked) if checked else False

                    item = next(filter(lambda i: i.get('path') == str(f), IMPORTS), None)
                    if item: item['selected'] = is_selected

                except Exception as error:
                    failed_imports.update(i.get('name'))                # OLD: print(f"ERROR: {error}")
            
            if len(failed_imports) > 0:
                failed_imports = ", ".join(failed_imports)
                #region Logger (error)
                LOGGER.new(
                    hdr="error",
                    msg=f"[IMPORT] Errors detected during import. The following files were skipped: {failed_imports}")
                #endregion
            
            APP.save_config(data=APP.CONFIG)

            return redirect(url_for('import_data'))
        
        elif 'export_distribution' in request.form:
            INPUT.tag = AppInputTag['IsExportActive']

            PARAMS = APP.CONFIG['exports']['DISTR']
            name_format = PARAMS.get('format', "")                      # TODO Add this to settings?

            FORMATS = { "timestamp": datetime.now().strftime('%y%m%d-%H%M%S') }
            
            INPUT.value = APP_MODULES.get('SPIDER', {})
            INPUT.value['filename'] = FORMATS.get(name_format, "")

        elif 'export_submit' in request.form:
            PARAMS = APP.CONFIG['exports']['DISTR']
            FOLDERS = { "SPIDER": APP.OUTPUT, "overwrite": APP.OVERWRITE }
            
            try:
                folder_input = request.form.get('export_submit')
                name_input = request.form.get('export_input')
                PREFIX = PARAMS.get('prefix') if PARAMS.get('prefix') else ""
                if PREFIX and name_input: PREFIX += "_"

                if not folder_input in FOLDERS:
                    raise KeyError(f"Invalid key: Selected folder ({folder_input}) is not valid or unknown")
                
                folder = FOLDERS.get(folder_input)

                if not folder:
                    raise ValueError(f"Invalid value: Path for the selected folder ({folder_input}) is not defined")
                
                # TODO Use APP_MODULES.get('SPIDER', {}).get('tag') instead of 'DISTR'?
                path = folder / f"{PREFIX}{name_input}_DISTR.ini"
                INPUT.tag = AppInputTag['SetExportPath']
                INPUT.value = { "path": path }
            
            except Exception as error:
                INPUT.value = { "path": None }            # if handled into /export/            --> only way that works

                #region Logger (error)                    # OLD: print(f"ERROR: {error}")
                LOGGER.new(
                    hdr="error",
                    msg=f"[EXPORT] {error}. Export has been aborted")
                #endregion

            return redirect(url_for('export_data'))       # if handled into /export/            --> only way that works

            # # if INPUT.tag and INPUT.value:                                                   # already tried to handle it here, didn't work
            # if INPUT.tag and INPUT.value.get('path'):
            #     return redirect(url_for('export_data'))
            
            # TODO Add a logger Error "error while saving: abortion" (into else statement (probably the best) or into except?)
            # TODO Set overwrite button as unavalaible into template if APP.OVERWRITE == None
        #endregion

        #region Settings Menu
        elif 'settings_close_app_submit' in request.form:
            INPUT.tag = AppInputTag['IsExitingApp']

            return jsonify({ 'App-Status': 'closed' })
        #endregion

        #region ToDo
        elif 'traits_save' in request.form: pass

        # DONE - changer dans rec.options.category, pour cat[record_index], et dans getattr(DISTRIBUTION, category)
        # DONE - pour uncategorized ==> rec.options.category = ''
        # TODO +++ ===> à ajouter comme feature lors du parsing d'un fichier ini car actuellement les records n'ont aucune categorie (et vont donc tous par défaut dans uncategorized ? En fait non car parsing se fait quand meme avec commentaires)

        # Loggers : StringFilters, FormFilters, Submit, Distribution - Categories, Distribution - Records
        #endregion

        else: app.config['USER_REQUESTS'].append('NOBODY')

        #region Popup & Logger initialization
        LOGGER._save_active()
        print(LOGGER)
        #endregion
    
    #region TESTING PURPOSE
    # # # # # # # # # # FOR TESTS # # # # # # # # # #
    # print(request.form)
    # # record_type = request.args.get('record_type')
    # # print(record_type)
    # print(request.args.__dict__)


    # print(app.config['TEST'])           # mkdir tes-vm-utils ???
    # from .models import Records, UserRecord
    # # records = Records().from_string(app.config['TEST'])
    # records = Records()
    # records.from_string(app.config['TEST'])
    # # record = UserRecord().from_string(app.config['RECTEST'])
    # # print(app.config['USER_RECORDS'])
    # # print(app.config.keys())
    # records = records._get_value()
    # print(records)
    # print("~~~~~~~~~~~~~~")
    # # # # # # # # # # /FOR TESTS # # # # # # # # # #
    #
    # # # # # # # # # FOR PATH CHECK # # # # # # # # #
    print("---------- APP infos ----------")
    print(f"MODS (MO2 mods root): {APP.MODS}")
    print(f"MO2 overwrite: {APP.OVERWRITE}")
    print(f"ROOT (SPIDER root): {APP.ROOT}")
    print(f"OUTPUT (SPIDER output): {APP.OUTPUT}")
    print(f"SETTINGS: {APP.SETTINGS}")
    print(f"LANGUAGES: {APP.LANGUAGES}")
    print(f"THEMES: {APP.THEMES}")
    print(f"CONFIG: {APP.CONFIG}")
    print(f"KEY: {APP.KEY}")
    print(f"URL: {APP.URL}")
    print("------------------------------")
    # # # # # # # # # /FOR PATH CHECK # # # # # # # # #
    #endregion

    return render_template('index.html',
                           AppInputTag=AppInputTag,
                           SubmitType=SubmitType,
                           record_types=RecordType, RecordIdType=RecordIdType,
                           LevelFilterType=LevelFilterType,
                           Skills=Skills,
                           Gender=Gender, Unique=Unique, Summonable=Summonable, Child=Child,
                           FormId=FormId,
                           navicons=RECORDTYPES_NAVICONS,
                           markers_colors=MARKERS_COLORS,
                           config=app.config['APP'].CONFIG,
                           input=app.config['INPUT'],
                           explorer=app.config['EXPLORER'],
                           user_records=app.config['USER_RECORDS'],
                           user_distribution=app.config['USER_DISTR'],
                           user_requests=app.config['USER_REQUESTS'],
                           user_selection=app.config['USER_SELECTION'],
                           logs=app.config['LOGS'])


@app.route('/files/', methods=['GET'])
def manage_files():
    app.config['INPUT'].tag = AppInputTag['IsImportActive']

    return redirect(url_for('index'))

#region EXIT APP - TEST
# exiting = False
# @app.route('/exit/', methods=['GET'])
# def close_app():
#     global exiting
#     exiting = True
#     return "Done"
#endregion


@app.route('/import/', methods=['GET'])
def import_data():

    APP = app.config['APP']
    DISTRIBUTION = app.config['USER_DISTR']
    IMPORTS = APP.CONFIG['imports']['DISTR']        # Old version: imports

    # if request.method == 'GET':
    files = [Path(i['path']) for i in IMPORTS if i.get('selected') and i.get('path')]           # OLD --> All SPID files: files = skyrim.get_spid_files(APP.MODS)
    content = skyrim.get_ini_contents(files=files, merge=True)

    if isinstance(content, str):
        records = Records()
        records.from_string(content)
        app.config['USER_DISTR'] = records
    
    return redirect(url_for('index'))


# @app.route('/export/', methods=['POST'])
@app.route('/export/', methods=['GET', 'POST'])     # Remettre que POST & le check associé
def export_data():                                  # Ajouter un bouton pour appeler la route en POST dans l'HTML
    # if request.method == 'POST':                  # Changer l'URL dans config & dans views côté serveur pour .../spider-app ou skyrim-tools.dochash.fr/spider (à voir sur OVH)

    APP = app.config['APP']
    INPUT = app.config['INPUT']
    DISTRIBUTION = app.config['USER_DISTR']         # Old version: records
    selected_path = INPUT.value.get('path')

    if selected_path:
        # Provide choice into app settings to save into SPIDER root, MO2 Overwrite folder, or custom path
        # TODO Disable all _DISTR.ini files (#old) and save their path to allow enabling them later

        if not selected_path:
            selected_path = APP.OUTPUT / "SPIDER_DISTR.ini"
            # TODO Log an Error to inform that there was an error with given path (invalid...), so path was autoset
            # TODO Or better to log an Error "error while saving: abortion"?
            #      Or to remove here, because handled into export_submit? --> doesn't work this way (already tried)

        data = ExportedData(
            data=DISTRIBUTION._get_value(),
            path=selected_path,
            url=f"{APP.URL[0]}{APP.URL[1]}"
            )
        
        is_sharing_enabled = True if APP.CONFIG.get('share_records') else False

        # if APP.CONFIG.get('share_records'):
        #     try:
        #         key_check = APP._check_key()
        #         is_sharing_enabled = True if key_check.get('allowed') else False
        #     except:
        #         is_sharing_enabled = False
        # else:
        #     is_sharing_enabled = False

        # data.export(dist=is_sharing_enabled)
        data.export(dist=is_sharing_enabled, hkey=APP.KEY)              # TODO Maybe useful to affect value for response to check if data were sent correctly?
    
    return redirect(url_for('index'))


@app.teardown_request
def teardown(exception):
    INPUT = app.config['INPUT']

    # Shutdown App
    if INPUT.tag == AppInputTag['IsExitingApp']:
        os._exit(0)















@app.route('/switch_record_type/')
def switch_record_type():
    print(app.config['USER_SELECTION'])
    app.config['USER_SELECTION']['record_type'] = 1
    print(app.config['USER_SELECTION'])
    return redirect(url_for('index'))



@app.route('/execute_code')
def execute_code():
    app.config['USER_ACTION'] = "append"
    return redirect(url_for('index'))
    # Code Python à exécuter ici
    # result = "Code exécuté avec succès !"
    # app.config['USER_REQUESTS'].append(result)
    # return render_template('index.html',
    #                        record_types=app.config['RECORD_TYPES'],
    #                        user_requests=app.config['USER_REQUESTS'])
    # redirige l'utilisateur vers la même page
    # return redirect(url_for('index'))

# @app.route('/execute_code')
# def execute_code():
#     # Code Python à exécuter ici
#     result = "Code exécuté avec succès !"
#     print(result)
#     app.config['USER_REQUESTS'].append(result)
#     return jsonify({'result': result})

# def execute_code():
#     # Code Python à exécuter ici
#     result = "Code exécuté avec succès !"
#     print(result)
#     app.config['USER_REQUESTS'].append(result)
#     # return result
#     return render_template('index.html',
#                            record_types=app.config['RECORD_TYPES'],
#                            user_requests=app.config['USER_REQUESTS'])


#region TESTS
@app.route('/post_data/', methods=['GET', 'POST'])
def post_data_test():
    # data = { "message": "hello world, posted data!" } # First test (with json)
    # data = "hello world, posted data!"                # First test (with text)

    # record = app.config['USER_RECORDS']               # Second test: Active record only
    # data = record._get_value()                        # Second test: Active record only

    records = app.config['USER_DISTR']                  # Third test: All records
    data = records._get_value()                         # Third test: All records

    # headers = {"Content-Type": "application/json"}          # for json
    headers = {"Content-Type": "text/plain"}                # for text

    url = "http://dochash.fr/tools/records/"
    # response = r.post(url=url, json=data, headers=headers)  # for json
    response = r.post(url=url, data=data, headers=headers)  # for text

    # return jsonify({"message": "posted data!", "data": data, "reponse_status": response.status_code})
    return redirect(url_for('index'))

# @app.route('/loading/', methods=['GET'])
# def load_page():
#     app.config['INPUT'].tag = AppInputTag['IsLoading']
#     return redirect(url_for('import_data'))

# import time
# @app.route('/simulate_long_operation/')
# def simulate_long_operation():
#     # Simuler une opération longue (par exemple, attendre 5 secondes)
#     # time.sleep(5)
#     for i in range(0, 100000):
#         print(i)
#     return 'Opération terminée'
#endregion