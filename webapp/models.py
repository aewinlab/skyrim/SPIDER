from dataclasses import dataclass, field
from enum import Enum
from pathlib import Path
from datetime import datetime
import re

# from flask import Response
from requests import Response
import requests as r

from skyrim_tools.lib.plugin import Mod
from skyrim_tools.utils.skyrimtools import get_plugins
from skyrim_tools import get_references
from .constants import RECORDTYPES_FILTERS
from .utils.file_io import save_data



#region Enums
class RecordType(Enum):
    Spell = 0
    Perk = 1
    Item = 2
    Shout = 3
    LevSpell = 4
    Package = 5
    Outfit = 6
    Keyword = 7
    DeathItem = 8
    Faction = 9
    SleepOutfit = 10
    Skin = 11

class Skills(Enum):
    OneHanded = 0
    TwoHanded = 1
    Marksman = 2
    Block = 3
    Smithing = 4
    HeavyArmor = 5
    LightArmor = 6
    Pickpocket = 7
    Lockpicking = 8
    Sneak = 9
    Alchemy = 10
    Speechcraft = 11
    Alteration = 12
    Conjuration = 13
    Destruction = 14
    Illusion = 15
    Restoration = 16
    Enchanting = 17
    NONE = 99

class RecordIdType(Enum):           #  UNUSED: NONE = 0
    FormID = 1                      # 'FormID~esp' eg '0x~mod'
    EditorID = 2                    # 'EditorID' eg 'stringName'

# class ModExtension(Enum):         # CURRENTLY UNUSED
#     ESP = 0
#     ESM = 1
#     ESL = 2

class LevelFilterType(Enum):
    NONE = 0
    ActorLevel = 1
    SkillLevel = 2

class Gender(Enum):
    NONE = 0
    M = 1
    F = 2

class Unique(Enum):
    NONE = 0
    U = 1

class Summonable(Enum):
    NONE = 0
    S = 1

class Child(Enum):
    NONE = 0
    C = 1

class SubmitType(Enum):
    NONE = 0
    Select = 1
    Enter = 2

class LogCategory(Enum):
    Null = -1
    Debug = 0
    Info = 1
    Error = 2

class AppInputTag(Enum):
    Null = 0
    IsImportActive = 1
    IsExportActive = 2
    IsTutorialActive = 9
    IsRenamingCategoryActive = 11
    IsChangingCategoryActive = 12
    SetExportPath = 21          # FOR WHAT DOES IT MATTER?
    IsSettingCustomPathActive = 81
    IsExitingApp = 91
    IsLoading = 99              # UNUSED?
#endregion


#region Logger
@dataclass
class Log:
    message: str = ""
    header: LogCategory = LogCategory['Null']
    timestamp: datetime = datetime.now()
    # is_displayed: bool = False

    def __post_init__(self) -> None:
        self.timestamp = datetime.now()

    def _jsonify(self) -> dict:
        return {
            'timestamp': self.timestamp.strftime('%d-%m-%Y %H:%M:%S'),
            'category': self.header.name,
            'message': self.message
        }
    
    def _get_time(self) -> str:
        return self.timestamp.strftime('%H:%M:%S')


@dataclass
class Logger:
    active: Log = field(default_factory=Log)
    content: list[Log] = field(default_factory=list)
    level: LogCategory = LogCategory['Debug']

    def new(self, msg: str = "", hdr: str = "null") -> Log:
        if msg and hdr.lower() == "null":
            hdr = self.level.name

        if hdr.lower() in [cat.name.lower() for cat in LogCategory]:
            self.active = Log(
                message=msg,
                header=LogCategory[hdr.lower().capitalize()],
                timestamp=datetime.now()
                )
        else: self.active = Log()

        return self.active
    
    def _save_active(self) -> None:
        if self.active.header.value >= self.level.value:
            self.content.append(self.active)
    
    def _jsonify(self) -> list:
        return [log._jsonify() for log in self.content]
#endregion


#region Data
@dataclass
class ExportedData:
    url: "str | None" = None
    time: "datetime | None" = None
    content_type: "str | None" = None
    data: "dict | str | None" = None

    path: "Path | None" = None

    # CURRENTLY UNUSED
    # request: Request                                      # from ReceivedRequest
    # ip: "str | None" = None                               # from ReceivedRequest
    # method: "str | None" = None                           # from ReceivedRequest
    # infos: "tuple[str]" = field(default_factory=tuple)

    def __post_init__(self) -> None:
        self.time = datetime.now()

        if type(self.data) == dict:                         # For json
            self.content_type = "application/json"
        elif type(self.data) == str:                        # For text
            self.content_type = "text/plain"
        else: return

    def export(self, local: bool = True, dist: bool = True, hkey: "str|None" = None) -> "Response|None":
        if not self.data: return

        if local:
            if not self.path: return
            save_data(data=self.data, file=self.path)

        if not dist: return
        if not self.url: return

        headers = {}
        if self.content_type: headers['Content-Type'] = self.content_type
        if hkey: headers['X-Api-Key'] = hkey

        try:
            response = r.post(
                url=self.url,
                data=self.data if self.content_type == "text/plain" else None,
                json=self.data if self.content_type == "application/json" else None,
                headers=headers
                )
            return response         # For debug: print(response.json())
            
        except:                     # TODO Handle Exception
            print("POST ERROR")
#endregion


#region App Inputs
@dataclass
class AppInput:
    tag: "AppInputTag | None" = None            # OR AppInputTag['Null'] ?
    value: "dict | None" = None

    def reset(self) -> None:
        self.tag = None
        self.value = None
#endregion


#region Mods/Records Explorer
@dataclass
class AppExplorer:
    mods: list[str] = field(default_factory=list)
    references: dict = field(default_factory=dict)
    filter: str = ""
    filtersets: list[str] = field(default_factory=list)

    # CURRENTLY UNUSED
    # ModList? / Mods / ModsData ? / ModReferences ? / ModListContent ?
    # path: Path
    # mods / modlist / detected_mods / detected : list[str]
    # selected_mods / selected / active: list[Mod]         --> mais contenu des mods aussi...
    # filters: list[str] = field(default_factory=list)
    # filter: ExplorerFilter = field(default_factory=ExplorerFilter)

    def __post_init__(self):
        self.set_filter(filter="Distributable")
        # TODO Get .esp/.esm.esl header to check (with try/except) if it's 'TES4' to clean modlist from non-plugin files (eg .esp/.esm/.esl files that are not mods; examples seen into node.js libraries)
    
    def parse_folder(self, folder: Path):
        self.mods = get_plugins(folder=folder, _sorted=True, _alpha_order=True)
        # NOTE Use get_plugins(folder=folder, sorted=True, fast_parser=False) to check recursively
        # TODO Add an option into settings to set it, for non-MO2 users?
        # TODO Add an option into settings to set alpha_order (let user choose)? Do the same for get_references below?
    
    def get_references(self, files: list[Path]):
        self.references = get_references(files=files, framework="spid")
    
    def set_filter(self, filter: str):
        if not filter in RECORDTYPES_FILTERS.keys(): return
        self.filter = filter
        matching_filters = RECORDTYPES_FILTERS.get(filter)
        if isinstance(matching_filters, list):
            self.filtersets = matching_filters
        elif isinstance(matching_filters, str):
            self.filtersets = [matching_filters]
#endregion


#region SPID Records
@dataclass
class FormId():
    value: str = ""                 # Hexadecimal value
    
    def __post_init__(self) -> None:
        self._format()
        self._check_format()

    def _format(self) -> None:
        self.value = f"{'0x' if not self.value.startswith('0x') else ''}{self.value.upper()}"
        self.value = f"0x{self.value[2:].lstrip('0')}"
        if len(self.value) < 3: self.value += "0"
    
    def _check_format(self) -> None:
        if len(self.value) > 8:
            raise ValueError('Invalid format: FormId length should not exceed 8 characters')
        if not re.match(r"[0-9A-Fa-f]{1,6}$", self.value[2:]):
            raise ValueError('Invalid format: FormId must start with "0x", followed by 1-6 characters consisting only of digits and letters from A to F')


@dataclass
class RecordId:
    id_type: RecordIdType = RecordIdType['FormID']
    editor_id: str = ""
    form_id: FormId = field(default_factory=FormId)
    mod: str = ""
    pattern: str = ""

    # # CURRENTLY UNUSED
    # mod_name: str = ""
    # mod_extension: ModExtension = ModExtension['ESP']

    def _get_value(self) -> str:
        if self.id_type.value == 1:
            return f"{self.form_id.value}~{self.mod}"
        
        return f"{self.pattern}{self.editor_id}"

    def switch_pattern(self, pattern: str) -> None:
        if pattern in self.pattern:
            self.pattern = self.pattern.strip(pattern)
        else: self.pattern += pattern
    
    def from_string(self, string: str):
        string = string.strip()
        PATTERNS = ("*",)

        if string.count("~") == 0:
            try:
                if len(string) < 1:                         # RecordID length is invalid (empty)
                    raise ValueError('Invalid value: RecordID length should be at least 1 character')
                
                self.id_type = RecordIdType['EditorID']
                
                if any(string.startswith(pattern) for pattern in PATTERNS):
                    self.pattern = string[0]

                i = 1 if self.pattern else 0
                self.editor_id = string[i:]

            except: return                                  # Record is invalid (RecordId)
            
        else:
            self.id_type = RecordIdType['FormID']
            record_parts = string.split("~")

            try:
                if len(record_parts) > 2:                   # FormId is invalid (several '~')
                    raise ValueError('Invalid format: Record (FormId) should contains only one "~" separator')
                
                self.form_id = FormId(record_parts[0])
                self.mod = record_parts[1]
                
            except: return                                  # Record is invalid (RecordId - FormId)
        
        return self


@dataclass
class StringFilter:
    index: int = 1
    strings: list[str] = field(default_factory=list)        # Elements will have "+" between them (ie. 'AND') ; default=[]
    pattern: str = ""                                       # If negated, str = "-"

    def _get_value(self) -> str:
        return f"{self.pattern}{'+'.join(self.strings)}"
    
    def switch_pattern(self, pattern: str) -> None:
        if pattern in self.pattern:
            self.pattern = self.pattern.strip(pattern)
        else: self.pattern += pattern
    
    def from_string(self, string: str):
        string = string.strip()
        PATTERNS = ("-",)

        try:
            if len(string) < 1:                             # Filter value length is invalid (empty)
                raise ValueError('Invalid value: StringFilter value length should be at least 1 character')
            
            if any(string.startswith(pattern) for pattern in PATTERNS): self.pattern = string[0]

            i = 1 if self.pattern else 0
            self.strings = [s.strip() for s in string[i:].split("+") if s.strip()]
            # Maybe remove "-" too, not only "" after strip()

            if not self.strings:                            # Filter value is invalid (empty)
                raise ValueError('Invalid value: StringFilter must contains at least 1 valid string')
            
        except: return                                      # Filter is invalid (StringFilter)

        return self


@dataclass
class StringFilters:
    content: list[StringFilter] = field(default_factory=list) # Default=[]
    active: int = 0

    def _get_value(self) -> str:
        if len(self.content) < 1: return "NONE"
        return ",".join([element._get_value() for element in self.content])
    
    def from_string(self, string: str):
        string = string.strip()

        if string == "NONE": return                         # Filter is null

        data = string.split(",")
        index = 1

        try:
            for i in data:
                string_filter = StringFilter()
                is_valid = string_filter.from_string(i)
                
                if not is_valid: continue                   # Or raise ValueError ?
                
                string_filter.index = index
                index += 1

                self.content.append(string_filter)

            if not self._get_value().strip(" -+,"):         # Filter value is invalid (empty)
                raise ValueError('Invalid value: StringFilters value must contains at least 1 valid filter')
            
        except: return                                      # Record is invalid (StringFilters)
        
        return self


@dataclass
class FormFilter:
    index: int = 1
    forms: list[RecordId] = field(default_factory=list)     # Elements will have "+" between them (ie. 'AND') ; default=[]
    pattern: str = ""                                       # If negated, str = "-"

    def _get_value(self) -> str:
        forms = [form._get_value() for form in self.forms]
        return f"{self.pattern}{'+'.join(forms)}"
    
    def switch_pattern(self, pattern: str) -> None:
        if pattern in self.pattern:
            self.pattern = self.pattern.strip(pattern)
        else: self.pattern += pattern
    
    def from_string(self, string: str):
        string = string.strip()
        PATTERNS = ("-",)

        try:
            if len(string) < 1:                             # Filter value length is invalid (empty)
                raise ValueError('Invalid value: FormFilter value length should be at least 1 character')
            
            if any(string.startswith(pattern) for pattern in PATTERNS): self.pattern = string[0]

            i = 1 if self.pattern else 0
            for s in string[i:].split("+"):
                if s.strip():
                    record = RecordId()
                    is_valid = record.from_string(s.strip())
                    if is_valid: self.forms.append(record)
                # Maybe remove "-" too, not only "" after s.strip()

            if not self.forms:                              # Filter value is invalid (empty)
                raise ValueError('Invalid value: FormFilter must contains at least 1 valid record')
            
        except: return                                      # Filter is invalid (FormFilter)

        return self


@dataclass
class FormFilters:
    content: list[FormFilter] = field(default_factory=list) # Default=[]
    active: int = 0
    new_form: "RecordId | None" = None

    def _get_value(self) -> str:
        if len(self.content) < 1: return "NONE"
        return ",".join([element._get_value() for element in self.content])
    
    def from_string(self, string: str):
        string = string.strip()

        if string == "NONE": return                         # Filter is null

        data = string.split(",")
        index = 1

        try:
            for i in data:
                form_filter = FormFilter()
                is_valid = form_filter.from_string(i)
                
                if not is_valid: continue                   # Or raise ValueError ?

                form_filter.index = index
                index += 1

                self.content.append(form_filter)

            if not self._get_value().strip(" -+,"):         # Filter value is invalid (empty)
                raise ValueError('Invalid value: FormFilters value must contains at least 1 valid filter')
            
        except: return                                      # Record is invalid (FormFilters)
        
        return self


@dataclass
class LevelFilters:
    filter_type: LevelFilterType = LevelFilterType['NONE']
    skill: Skills = Skills['NONE']
    bounds: tuple[str, str] = ("0", "")

    def _get_value(self) -> str:
        """Computes and returns the correct "LevelFilters" value for a SPID record."""

        if self.filter_type.value == 0: return "NONE"
        
        output = [f"{'/'.join(self.bounds).rstrip('/')}"]
        if self.filter_type.value == 2:
            output.insert(0, f"{self.skill.value}(")
            output.append(")")
        return "".join(output)
    
    def from_string(self, string: str):
        string = string.strip()

        if string == "NONE": return                                     # Filter is null
        if string.count("(") > 1 or string.count(")") > 1: return       # Filter structure is invalid
        if string.count("(") != string.count(")"): return               # Filter structure is invalid

        data = string.strip("()").partition("(")

        if data[1] == "(": self.filter_type = LevelFilterType['SkillLevel']
        else: self.filter_type = LevelFilterType['ActorLevel']

        skill = data[0] if self.filter_type == LevelFilterType['SkillLevel'] else "99"
        bounds = data[2] if self.filter_type == LevelFilterType['SkillLevel'] else data[0]
        bounds = bounds.partition("/")

        try:                                                # Filter values are invalid
            if not skill.isdigit():
                raise ValueError('Invalid value: LevelFilter skill value should be digits')
            if not bounds[0].isdigit():
                raise ValueError('Invalid value: LevelFilter lower bound value should be digits')
            if not (bounds[2].isdigit() or bounds[2] == ""):
                raise ValueError('Invalid value: LevelFilter upper bound value should be digits or ""')
            if not int(skill) in [i.value for i in Skills]:
                raise ValueError('Invalid value: LevelFilter skill value be a number between 0 and 17')
            if bounds[2].isdigit() and int(bounds[0]) > int(bounds[2]):
                raise ValueError('Invalid value: LevelFilter upper bound value should be higher than lower bound')

            self.skill = Skills(int(skill))
            self.bounds = (bounds[0], bounds[2])

        except: return                                      # Record is invalid (LevelFilters)

        return self


@dataclass
class Traits:
    gender: tuple[str, Gender] = ("", Gender['NONE'])               # If negated, str = "-"
    unique: tuple[str, Unique] = ("", Unique['NONE'])               # If negated, str = "-"
    summonable: tuple[str, Summonable] = ("", Summonable['NONE'])   # If negated, str = "-"
    child: tuple[str, Child] = ("", Child['NONE'])                  # If negated, str = "-"

    def _get_value(self) -> str:
        """Computes and returns the correct "Traits" value for a SPID record."""

        if self.gender[1].value + self.unique[1].value + self.summonable[1].value + self.child[1].value == 0:
            return "NONE"
        
        all_traits = []
        for attr in self.__dict__:
            if self.__dict__[attr][1].value == 0: continue
            attr_value = f"{self.__dict__[attr][0]}{self.__dict__[attr][1].name}"
            all_traits.append(attr_value)
        return "/".join(all_traits)
    
    def from_string(self, string: str):
        string = string.strip()

        if string == "NONE": return                         # Filter is null

        TRAITS = { "M": Gender, "F": Gender, "U": Unique, "S": Summonable, "C": Child }
        data = string.split("/")

        try:
            for i in data:
                if len(i) < 1 or len(i) > 2 :               # Trait string length is invalid
                    raise ValueError('Invalid value: Trait value length should be 1 or 2 characters')
                
                value = i[1] if len(i) == 2 else i[0]
                trait = TRAITS.get(value)                   # OLD: TRAITS.get(value) if len(i) == 2 else TRAITS.get(value)
                pattern = i[0] if len(i) == 2 else ""

                if not trait:                               # Trait value is invalid
                    raise ValueError('Invalid value: Trait value is unknown')
                if not pattern in ["-", ""]:                # Pattern value is invalid
                    raise ValueError('Invalid value: Trait pattern must be "-" or nothing')
                
                attr = trait.__name__.lower()

                if not hasattr(self, attr):                 # Trait value is invlaid (no matching attribute)
                    raise ValueError('Invalid value: Trait type is unknown')
                
                setattr(self, attr, (pattern, trait[value]))

        except: return                                      # Record is invalid (Traits)

        return self


@dataclass
class RecordOptions:
    submit_type: SubmitType = SubmitType['NONE']
    category: str = ""

    # Content to add here?


@dataclass
class UserRecord:
    record_type: RecordType = RecordType['Spell']
    id: RecordId = field(default_factory=RecordId)
    strings: StringFilters = field(default_factory=StringFilters)
    forms: FormFilters = field(default_factory=FormFilters)
    lvl: LevelFilters = field(default_factory=LevelFilters)
    traits: Traits = field(default_factory=Traits)
    item_count: int = 1
    package_idx: int = 0
    chance: int = 100

    invalid: tuple[bool, list[str]] = (False, [])
    options: RecordOptions = field(default_factory=RecordOptions)       # OLD: = RecordOptions() --> issue where every records had the same RecordOptions when record.options was changed for a record

    # Valid Record structures are:
    # - RecordType = RecordID|StringFilters|FormFilters|LevelFilters|Traits|NONE|Chance
    # - Item = RecordID|StringFilters|FormFilters|LevelFilters|Traits|ItemCount|Chance
    # - Package = RecordID|StringFilters|FormFilters|LevelFilters|Traits|PackageIdx|Chance

    def _get_varfield_value(self) -> "str | None":
        if self.record_type == RecordType['Item']:
            if self.chance == 100 and self.item_count == 1: return
            return str(self.item_count)

        elif self.record_type == RecordType['Package']:
            if self.chance == 100 and self.package_idx == 0: return
            return str(self.package_idx)

        return "NONE"
    
    def _get_value(self) -> str:
        """Computes and returns the correct final value for a SPID record."""

        record_values = [self.__dict__[attr]._get_value() for attr in ['id', 'strings', 'forms', 'lvl', 'traits']]

        varfield_value = self._get_varfield_value()
        if varfield_value: record_values.append(varfield_value)

        if self.chance != 100: record_values.append(str(self.chance))

        return f"{self.record_type.name} = {'|'.join(record_values).rstrip('|NONE')}"
    
    def from_string(self, string: str):
        if string.count("=") != 1: return                           # Record structure is invalid

        record_type = string.partition("=")[0].strip()
        record_data = string.partition("=")[2].strip()

        # if record_type not in [i.name for i in RecordType]: return    # RecordType is invalid

        try:
            if record_type not in [i.name for i in RecordType]:     # RecordType is invalid
                raise TypeError(f'Invalid type: {record_type} is not a valid RecordType')
            self.record_type = RecordType[record_type]
        except: return

        record_attrs = record_data.split("|")

        ATTRS = {
            "id": RecordId,
            "strings": StringFilters,
            "forms": FormFilters,
            "lvl": LevelFilters,
            "traits": Traits,
            ("item_count" if record_type == "Item"          # Possible values for varfield:
            else "package_idx" if record_type == "Package"  # "item_count", "package_idx"
            else "varfield"): None,                         # or "NONE" for others RecordTypes
            "chance": None
            }

        data = { attr: value.strip() for attr, value in zip(ATTRS.keys(), record_attrs) }

        for attr in data:
            if attr == "varfield": continue

            if attr in ["chance", "item_count", "package_idx"]:
                try:                                        # Record value is invalid (all <0, chance >100)
                    value = int(data[attr].strip())
                    if value < 0:
                        raise ValueError(f'Invalid value: {attr.capitalize()} value should be at least 0')
                    if attr == "chance" and value > 100:
                        raise ValueError(f'Invalid value: {attr.capitalize()} value should not exceed 100')
                    setattr(self, attr, value)
                except: self.invalid = (True, self.invalid[1] + [attr])
                continue

            try:
                value = ATTRS[attr]()
                is_valid = True if data[attr].strip() == "NONE" else value.from_string(data[attr])

                if not is_valid:                            # Record value is invalid
                    raise ValueError(f'Invalid value: String given for {attr.capitalize()} is not valid')

                setattr(self, attr, value if value else ATTRS[attr]())
            except: self.invalid = (True, self.invalid[1] + [attr])

        # self.record_type = RecordType[record_type]
        # print(self)                                       # DEBUG
        # return self


@dataclass
class RecordCategory:
    content: list[UserRecord] = field(default_factory=list)
    collapsed: bool = False


@dataclass
class Records:
    uncategorized: RecordCategory = field(default_factory=RecordCategory)       # OLD: = RecordCategory() --> issue where records where added at every import
    categories: list[str] = field(default_factory=list)

    def _new_category(self, name: str) -> None:
        if name == "": return                               # Corresponds to Uncategorized

        if not (name.lower() in self.categories and hasattr(self, name)):
            setattr(self, name, RecordCategory())           # ALT: self.__setattr__(name, [])
            self.categories.append(name)                    # OR self.__dict__[name] = []
    
    def _del_category(self, name: str) -> None:
        if name == "": return                               # Corresponds to Uncategorized

        if name.lower() in self.categories and hasattr(self, name):
            delattr(self, name)
            self.categories.remove(name)
    
    def _rename_category(self, current_name: str, new_name: str) -> None:
        if current_name == "" or new_name == "": return     # Corresponds to Uncategorized

        # Category already existing (new name already set)
        if new_name in self.categories or hasattr(self, new_name): return

        if current_name in self.categories and hasattr(self, current_name):
            i = self.categories.index(current_name)
            self.categories.insert(i, new_name)
            setattr(self, new_name, getattr(self, current_name))
            self._del_category(current_name)

            category = getattr(self, new_name)
            for record in category.content:
                record.options.category = new_name
        
    def _get_record(self, index: int, category: str, extract: bool = False) -> "UserRecord | None":
        if not hasattr(self, category.lower()): return                      # Or raise AttributeError() or KeyError()?

        category_records = getattr(self, category.lower()).content
        if len(category_records) < 1: return
        if index < 0 or index > len(category_records) - 1: return           # Or raise ValueError() : index out of range

        return category_records.pop(index) if extract else category_records[index]
    
    def _add_record(self, record: UserRecord, category: "str | None" = None) -> None:
        if category:
            if category in self.categories and hasattr(self, category):
                getattr(self, category).content.append(record)
            else:
                raise ValueError(f"Invalid category name: {category}")
        else: self.uncategorized.content.append(record)
    
    def _del_record(self, record: UserRecord, category: str) -> None:
        if not hasattr(self, category.lower()):
            raise ValueError(f"Invalid category name: {category}")
        if not record in getattr(self, category.lower()).content:
            raise ValueError(f"Invalid record: {record._get_value()} not found in category {category}")
        
        getattr(self, category.lower()).content.remove(record)
    
    def _change_record_category(self, index: int, current_category: str, new_category: str) -> None:
        if new_category.lower() == current_category.lower(): return             # no change made (same category)
        if new_category.lower() == "uncategorized": new_category = ""

        record = self._get_record(index=index, category=current_category, extract=True)

        if not record:
            raise ValueError()                  # TODO Handle error --> no record found
        
        record.options.category = new_category
        self._add_record(record=record, category=new_category)

    def _collapse_or_expand_category(self, name: str) -> None:
        if name.lower() in self.categories and hasattr(self, name):
            getattr(self, name).collapsed = not getattr(self, name).collapsed
        elif name.lower() == "uncategorized":
            self.uncategorized.collapsed = not self.uncategorized.collapsed
    
    def _move_up_category(self, name: str) -> None:
        if name.lower() in self.categories and hasattr(self, name):
            i = self.categories.index(name)
            if len(self.categories) > 0 and i > 0:
                self.categories[i], self.categories[i - 1] = self.categories[i - 1], self.categories[i]
        elif name.lower() == "uncategorized": return
    
    def _move_down_category(self, name: str) -> None:
        if name.lower() in self.categories and hasattr(self, name):
            i = self.categories.index(name)
            if len(self.categories) > 0 and i < len(self.categories) - 1:
                self.categories[i], self.categories[i + 1] = self.categories[i + 1], self.categories[i]
        elif name.lower() == "uncategorized": return

    def _move_up_record(self, index: int, category: str) -> None:
        if hasattr(self, category.lower()):
            category_records = getattr(self, category.lower()).content
            if len(category_records) > 0 and index > 0:
                record = category_records[index]
                previous_record = category_records[index - 1]
                category_records[index], category_records[index - 1] = previous_record, record

    def _move_down_record(self, index: int, category: str) -> None:
        if hasattr(self, category.lower()):
            category_records = getattr(self, category.lower()).content
            if len(category_records) > 0 and index < len(category_records) - 1:
                record = category_records[index]
                next_record = category_records[index + 1]
                category_records[index], category_records[index + 1] = next_record, record
    
    def _remove_record(self, index: int, category: str) -> None:
        record = self._get_record(index=index, category=category)
        if record: self._del_record(record=record, category=category)
    
    def _get_value(self) -> str:
        distribution_value = ""
        
        for category in self.categories + ["uncategorized"]:
            name = category.capitalize()
            records = '\n'.join(
                [record._get_value() for record in getattr(self, category).content]
                )
            distribution_value += f";  [[ CATEGORY: {name} ]]\n\n{records}\n\n\n\n"
        
        return distribution_value.strip("\n ")
    
    def from_string(self, string: str):
        lines = string.split("\n")
        category_name = ""                                  # ALT: active_category = "uncategorized"

        for line in lines:
            line = line.strip(" ")
            if line == "": continue

            if line.startswith(";"):                        # It's a comment or a category
                start = line.find("[[ CATEGORY:")
                end = line.find("]]")

                if any(i for i in (start, end)) == -1 or end <= start + 12:
                    print("COMMENT")                                        # It's a comment
                    # Do something
                    continue
                
                # Check if category name is "Uncategorized"
                if line[start + 12:end].strip().lower() == "uncategorized":
                    category_name = ""
                    continue

                category_name = line[start + 12:end].strip().lower()        # It's a category
                self._new_category(name=category_name)
                continue
            
            try:                                            # It's a record
                record = UserRecord()
                record.from_string(line)                    # ALT: is_valid = record.from_string(line)

                is_valid = True if record.invalid[0] == False else False

                if not is_valid:
                    raise ValueError(f'Invalid value: Line "{line}" is not a valid record (invalid {", ".join(record.invalid[1])})')
                
                if not hasattr(self, category_name) and category_name:
                    raise ValueError(f'Invalid value: "{category_name.capitalize()}" is not a valid category')
                
                record.options.category = category_name

                self._add_record(
                    record=record,
                    category=record.options.category if record.options.category != "" else None
                    )
            
            except: continue
        
        # return self
#endregion
