

TRAITS_INPUTS = (
    'gender_input',
    'unique_input',
    'summonable_input',
    'child_input',
    'gender_mod_input',
    'unique_mod_input',
    'summonable_mod_input',
    'child_mod_input'
    )


TUTORIALS = {                                                       # TODO Add correct urls
    "PLAYLIST": "https://www.youtube.com/embed/videoseries?si=7rlUJ8tWUGr_MoPx&amp;list=PLuWIjlx3PFSX_G7WL3qbHR2ZBIcfAtc53",
    "id_help": "https://www.youtube.com/embed/b53zJzQJYIw",
    "string_filters_help": "",
    "form_filters_help": "",
    "level_help": "https://www.youtube.com/embed/60qgBsPN5JA",
    "traits_help": "https://www.youtube.com/embed/8caLJBH7EQU",
    "variable_help": "https://www.youtube.com/embed/yG6pqJYJOpc",
    "chance_help": "https://www.youtube.com/embed/-YXnvRl2U5o",
    "submit_help": "",
    "lateral_menu_help": ""
}
# "PLAYLIST_src": "https://www.youtube.com/playlist?list=PLuWIjlx3PFSX_G7WL3qbHR2ZBIcfAtc53",
# "general_presentation": "https://www.youtube.com/embed/VLd_c-8m91g",


IMPORTS_KEYS = {
    "path",
    "name",
    "selected",
    "disabled"
    }


APP_MODULES = {
    "SPIDER": {
        "name": "SPIDER",
        "img": "img/spider-solid.svg",
        "tag": "DISTR"
    }
}


RECORDTYPES_NAVICONS = {
    "base": {
        "Spell": "img/nav/fire-flame-curved-solid.svg",             # TODO Replace by a mix with bolt-solid (cf 'TEMPORARY-...')
        "Perk": "img/nav/diagram-project-solid.svg",
        "Item": "img/nav/coins-solid.svg",                          # TODO Replace by a mix with ring/shield-halved
        "Shout": "img/nav/head-side-cough-solid.svg",
        "LevSpell": "img/nav/bolt-solid.svg",                       # TODO Replace by a mix of Spell + table/clipboard/list icon (to download)
        "Package": "img/nav/masks-theater-solid.svg",
        "Outfit": "img/nav/shirt-solid.svg",
        "Keyword": "img/nav/key-solid.svg",                         # Another better?
        "DeathItem": "img/nav/skull-solid.svg",
        "Faction": "img/nav/flag-solid.svg",
        "SleepOutfit": "img/nav/bed-solid.svg",
        "Skin": "img/nav/person-half-dress-solid.svg"
    },
    "active": {
        "Spell": "img/nav/fire-flame-curved-solid-highlight.svg",   # TODO Replace by a mix with bolt-solid (cf 'TEMPORARY-...')
        "Perk": "img/nav/diagram-project-solid-highlight.svg",
        "Item": "img/nav/coins-solid-highlight.svg",                # TODO Replace by a mix with ring/shield-halved
        "Shout": "img/nav/head-side-cough-solid-highlight.svg",
        "LevSpell": "img/nav/bolt-solid-highlight.svg",             # TODO Replace by a mix of Spell + table/clipboard/list icon (to download)
        "Package": "img/nav/masks-theater-solid-highlight.svg",
        "Outfit": "img/nav/shirt-solid-highlight.svg",
        "Keyword": "img/nav/key-solid-highlight.svg",               # Another better?
        "DeathItem": "img/nav/skull-solid-highlight.svg",
        "Faction": "img/nav/flag-solid-highlight.svg",
        "SleepOutfit": "img/nav/bed-solid-highlight.svg",
        "Skin": "img/nav/person-half-dress-solid-highlight.svg"
    }
}

RECORDTYPES_FILTERS = {
    'Spell': 'spell',
    'Perk': 'perk',
    'Item': 'item',
    'Shout': 'shout',
    'LevSpell': 'levspell',
    'Package': 'package',
    'Outfit': 'outfit',
    'Keyword': 'keyword',
    'DeathItem': 'item',
    'Faction': 'faction',
    'SleepOutfit': 'outfit',
    'Skin': 'skin',
    'Actor': 'actor',
    'Distributable': ['spell', 'perk', 'item', 'shout', 'levspell', 'package', 'outfit', 'keyword', 'faction', 'skin'],
    'None': ['spell', 'perk', 'item', 'shout', 'levspell', 'package', 'outfit', 'keyword', 'faction', 'skin', 'actor']
}

MARKERS_COLORS = {
    'spell': 'bg-sapphire',
    'perk': 'bg-emerald',
    'item': 'bg-topaz',
    'shout': 'bg-turquoise',
    'levspell': 'bg-aquamarine',
    'package': 'bg-ruby',
    'outfit': 'bg-amethyst',
    'keyword': 'bg-garnet',
    'deathitem': 'bg-topaz',                                        # bg-citrine
    'faction': 'bg-sardonyx',
    'sleepoutfit': 'bg-amethyst',                                   # bg-rhodolite
    'skin': 'bg-rhodolite',                                         # bg-diamond
    'actor': 'bg-diamond',
    'distributable': 'bg-active',
    'none': 'bg-deep'
}

# # # # # # # # # # # # # # # INFOS ABOUT CATEGORIES/RECORDTYPES # # # # # # # # # # # # # # #
# Skins : ARMO, avec SkinXXX comme EDID et RNAM (race data) != DefaultRace "Default Race" (RACE:0x19)
# SleepOutfit : OTFT, avec Sleep dedans (ex DefaultSleepOutfit) et attribué via SOFT (sleeping outfit) dans record des NPCs
# DeathItem : item, avec DeathItemXXX comme EDID et attribué via INAM (death item) dans record des NPCs
# # # # # # # # # # # # # # # # # # # # # # #    # # # # # # # # # # # # # # # # # # # # # # #