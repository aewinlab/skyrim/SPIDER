from pathlib import Path

from flask import Flask
# from flask_sqlalchemy import SQLAlchemy


# app = Flask(__name__, static_folder='static')
# app = Flask(__name__, static_url_path='/static')
app = Flask(__name__)
app.config.from_object('config')

# db = SQLAlchemy(app)

from . import views



#region # # # # # # # FOR TESTS # # # # # # # # # #
# # ##### TEST WITH FAKE DATA
# import infos
# app.config['TEST'] = infos.test
# app.config['RECTEST'] = infos.rectest


# # ##### GET SPID file(s) content
# TODO Do this at launch depending on a setting?
# import skyrim_tools.utils.skyrimtools as skyrim
# from . import models
# APP = app.config['APP']
# file = APP.ROOT / "SPIDER_DISTR.ini"
# files = skyrim.get_spid_files(APP.MODS)
# content = skyrim.get_ini_contents(files=files, merge=True)
# if isinstance(content, str):
#     records = models.Records()
#     records.from_string(content)
#     app.config['USER_DISTR'] = records
#endregion
