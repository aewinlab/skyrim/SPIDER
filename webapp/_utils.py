from enum import EnumMeta
from typing import Type

from flask import request

from . import app
from .models import Skills, RecordId, FormId, RecordIdType, StringFilter, FormFilter, StringFilters, FormFilters


# rework docstring + comments
def clean_strings(input: list[str], chars: str = " +-") -> list:
    return [item.strip(chars) for item in input if item.strip(chars) != ""]


#rework docstring + comments
def form_to_tuple(string: str) -> tuple:
    data = [_.strip(" ") for _ in string.strip("()").split(',')]

    if len(data) < 1: return ()

    formatted_data = []

    for item in data:
        if item.isdigit(): formatted_data.append(int(item))
        elif item.isdecimal(): formatted_data.append(float(item))
        elif item.startswith("'") and item.endswith("'"): formatted_data.append(item.strip("'"))
        elif item.startswith('"') and item.endswith('"'): formatted_data.append(item.strip('"'))
        else: raise TypeError(f"Invalid type: {item}")
    
    return tuple(formatted_data)


# rework docstring + comments
def switch_trait_value(trait: EnumMeta) -> tuple:
    """Switch the value (which tuple index is i) of a given trait to the next possible value in their enum."""

    trait_value = app.config['USER_RECORDS'].traits.__dict__[trait.__name__.lower()]
    CONSTANT = trait_value[0]

    possible_values = list(trait)
    current_index = possible_values.index(trait_value[1])
    next_index = (current_index + 1) % len(possible_values)

    if trait_value[0] != "" and possible_values[next_index] == trait['NONE']:
        return (switch_trait_modifier(trait)[0], possible_values[next_index])

    return (CONSTANT, possible_values[next_index])


# rework docstring + comments
def switch_trait_modifier(trait: EnumMeta) -> tuple:

    trait_value = app.config['USER_RECORDS'].traits.__dict__[trait.__name__.lower()]
    CONSTANT = trait_value[1]

    possible_values = ["", "-"]
    current_index = possible_values.index(trait_value[0])
    next_index = (current_index + 1) % len(possible_values)

    if trait_value[1] == trait['NONE'] and trait_value[0] == "":
        return (possible_values[next_index], switch_trait_value(trait)[1])

    # app.config['USER_RECORDS'].traits.__dict__[trait.__name__.lower()] = (possible_values[next_index], CONSTANT)
    return (possible_values[next_index], CONSTANT)


# rework docstring + comments
def get_level_filters() -> dict:
    skill_input = request.form.get('skill_input')
    lvl_min_input = request.form.get('lvl_min_input')
    lvl_max_input = request.form.get('lvl_max_input')

    if not lvl_min_input: lvl_min_input = "0"
    if not lvl_max_input: lvl_max_input = ""

    # PENSER A GERER LES ERREURS DE TYPE (ISDIGIT POUR LES LVL_MIN ET LVL_MAX) MEME SI VERIFIE PAR FORM

    return {
        'skill': Skills[skill_input] if skill_input else Skills['NONE'],
        'bounds': (lvl_min_input, lvl_max_input)
    }


# rework docstring + comments
# def get_record_id() -> dict:
def get_record_id(src: "RecordId | None" = None) -> dict:
# def get_record_id(filters: str) -> dict:                  # filters ou record ou card ou widget ?
#     if not filters in ['id', 'forms']: return
#     id_type = app.config['USER_RECORDS'].__dict__[filters].id_type
    id_type = src.id_type if src else app.config['USER_RECORDS'].id.id_type
    id_input = request.form.get('id_input')
    mod_input = request.form.get('mod_input')

    # DEBUG
    # print("id_type (", type(id_type), "):", id_type)
    # print("id (", type(id_input), "):", id_input)
    # print("mod (", type(mod_input), "):", mod_input)

    if not id_input: id_input = ""
    if not mod_input: mod_input = "Skyrim.esm"                  # Or "" ?
    
    if id_type == RecordIdType['FormID']:
        try: id_input = FormId(id_input)
        except: id_input = FormId()
        # Penser à gérer les erreurs, ajouter un USER_ERROR dans config et faire apparaître avec une anim CSS un popup de message d'erreur ?

    return {
        'id': id_input,
        'mod': mod_input
    }


# rework docstring + comments
def set_active_filter(filters: str, index: "str | None"):
    if not filters in ['strings', 'forms']: return

    active_index = app.config['USER_RECORDS'].__dict__[filters].active

    if index and index.isdigit():
        if int(index) == active_index: app.config['USER_RECORDS'].__dict__[filters].active = 0
        else: app.config['USER_RECORDS'].__dict__[filters].active = int(index)


# rework docstring + comments
def add_new_filter(filters: str, filter_type: Type["StringFilter | FormFilter"]):
    # def add_filter(filters: str):
    if not filters in ['strings', 'forms']: return

    # filter_type = FormFilter if filters == "forms" else StringFilter
    # filter_type = [StringFilter, FormFilter][['strings', 'forms'].index(filters)]
    # if filters == "strings": filter_type = StringFilter
    # elif filters == "forms": filter_type = FormFilter
    # else: return

    new_index = len(app.config['USER_RECORDS'].__dict__[filters].content) + 1
    app.config['USER_RECORDS'].__dict__[filters].content.append(filter_type(index=new_index))


# rework docstring + comments
def remove_active_filter(filters: str) -> tuple:
    active_index = app.config['USER_RECORDS'].__dict__[filters].active

    if active_index == 0: return ("", -1)
    
    removed_filter = app.config['USER_RECORDS'].__dict__[filters].content.pop(active_index - 1)
    remaining_filters = app.config['USER_RECORDS'].__dict__[filters].content

    if len(remaining_filters) < 1:
        app.config['USER_RECORDS'].__dict__[filters].active = 0
    elif active_index > len(remaining_filters):
        app.config['USER_RECORDS'].__dict__[filters].active = len(remaining_filters)

    for item in app.config['USER_RECORDS'].__dict__[filters].content:
        item.index = app.config['USER_RECORDS'].__dict__[filters].content.index(item) + 1
    
    return (removed_filter, removed_filter.index)

# def switch_filter_pattern(filters: str):
#     if not filters in ['strings', 'forms']: return

#     active_index = app.config['USER_RECORDS'].__dict__[filters].active

#     if active_index != 0:

#         if filters == 'forms': current_pattern = app.config['USER_RECORDS'].forms.new_form.pattern
#         else: current_pattern = app.config['USER_RECORDS'].strings.content[active_index - 1].pattern

#         if current_pattern:
#             if filters == 'forms': app.config['USER_RECORDS'].forms.new_form.pattern = ""
#             else: app.config['USER_RECORDS'].strings.content[active_index - 1].pattern = ""
#         else:
#             if filters == 'forms': app.config['USER_RECORDS'].forms.new_form.pattern = "*"
#             else: app.config['USER_RECORDS'].strings.content[active_index - 1].pattern = "-"

# def update_filter_pattern(filter_pattern: str, pattern: str):
#     if pattern in filter_pattern:
#         return filter_pattern.strip(pattern)
#     # else: filter_pattern = f"{filter_pattern}{pattern}"
#     return f"{filter_pattern}{pattern}"
#     # else: filter_pattern += pattern

# TO REWORK ???
# rework docstring + comments
def add_string_to_filter() -> tuple:
    active_index = app.config['USER_RECORDS'].strings.active
    string_input = request.form.get('string_input')

    # ajouter de quoi tester le string_input pour éliminer input vides (p ex "   ") ?

    if string_input and active_index > 0:
        app.config['USER_RECORDS'].strings.content[active_index - 1].strings.append(string_input)

        # Logger (debug)
        # app.config['LOGS'].new(hdr="debug", msg=f"Added a new string ({string_input}) to String Filter #{active_index}")

        return (string_input, active_index)
    
    # Logger (debug)
    # app.config['LOGS'].new(hdr="debug", msg=f"No string added to String Filter #{active_index} (no string input)")

    return ("", -1)

# rework docstring + comments
def add_to_filter(filters: str, input: "str | RecordId") -> tuple:
    if not filters in ['strings', 'forms']: return ("", -1)

    active_index = app.config['USER_RECORDS'].__dict__[filters].active
    # string_input = request.form.get('string_input')

    # ajouter de quoi tester le string_input pour éliminer input vides (p ex "   ") ?

    if input and active_index > 0:
        app.config['USER_RECORDS'].__dict__[filters].content[active_index - 1].__dict__[filters].append(input)

        # Logger (debug)
        # app.config['LOGS'].new(hdr="debug", msg=f"Added a new string ({string_input}) to String Filter #{active_index}")

        return (input, active_index)
    
    # Logger (debug)
    # app.config['LOGS'].new(hdr="debug", msg=f"No string added to String Filter #{active_index} (no string input)")

    return ("", -1)



# CURRENTLY UNUSED - TO REMOVE ??? (récupérer les logger ?)
# rework docstring + comments
def remove_string_filter() -> tuple:
    active_index = app.config['USER_RECORDS'].strings.active

    if active_index == 0:
        
        # Logger (debug)
        # app.config['LOGS'].new(hdr="debug", msg=f"No filter removed from String Filters (no filter selected)")

        return ("", -1)
    
    removed_filter = app.config['USER_RECORDS'].strings.content.pop(active_index - 1)
    # if active_index != 0:
    #     removed_filter = app.config['USER_RECORDS'].strings.content.pop(active_index - 1)

    if active_index == 0 or len(app.config['USER_RECORDS'].strings.content) < 1:
        app.config['USER_RECORDS'].strings.active = 0
    elif active_index > len(app.config['USER_RECORDS'].strings.content):
        app.config['USER_RECORDS'].strings.active = len(app.config['USER_RECORDS'].strings.content)

    for item in app.config['USER_RECORDS'].strings.content:
        item.index = app.config['USER_RECORDS'].strings.content.index(item) + 1
    
    # Logger (debug)
    # app.config['LOGS'].new(hdr="debug", msg=f"Removed filter #{removed_filter.index} {'(' if len(removed_filter.strings) > 0 else ''}{removed_filter.pattern}{'+'.join(removed_filter.strings)}{') ' if len(removed_filter.strings) > 0 else ''}from String Filters")
    
    return (removed_filter, removed_filter.index)
    

def get_pattern(string: str, patterns: list[str] = ["-"]) -> dict:
    pattern = ""

    if len(string) > 0 and string[0] in patterns:
        pattern = string[0]
        string = string[1:]
    
    return {
        'pattern': pattern,
        'string': string
    }

def get_filter_content(string: str, patterns: list[str] = ["-"]) -> dict:

    filter = get_pattern(string, patterns)
    raw_strings = filter.get('string')
    strings = clean_strings(raw_strings.split("+")) if raw_strings else []

    return {
        'strings': strings,
        'pattern': filter.get('pattern'),
        'is_valid': True if strings else False
    }

def get_filters_content(string: str, patterns: list[str] = ["-"]) -> list[dict]:

    filters_list = string.split(",")
    filters_content = []

    for filter in filters_list:
        filter_content = get_filter_content(filter, patterns)

        if filter_content.get('is_valid'):
            filter_content['index'] = len(filters_content) + 1
            filters_content.append(filter_content)

    return filters_content

# TO REWORK ???
# def get_string_filter(string: str) -> "StringFilter | None":
def get_string_filter(string: str) -> StringFilter:

    filter_content = get_filter_content(string)

    print("HERE: ", filter_content)

    # if not (filter_content.get('is_valid') or filter_content.get('strings') or filter_content.get('pattern')):
    if not (filter_content.get('is_valid') or filter_content.get('strings') or filter_content.get('pattern')):
        # return None
        print("ICIIIIII")
        return StringFilter()
    
    return StringFilter(strings=filter_content['strings'],
                        pattern=filter_content['pattern'])


def get_form_filter(string: str) -> FormFilter:

    filter_content = get_filter_content(string)

    print("HERE: ", filter_content)

    # if not (filter_content.get('is_valid') or filter_content.get('strings') or filter_content.get('pattern')):
    if not (filter_content.get('is_valid') or filter_content.get('strings') or filter_content.get('pattern')):
        # return None
        print("ICIIIIII")
        return FormFilter()
    
    return FormFilter(
        forms=[get_record_id_from_string(string) for string in filter_content['strings']],
        pattern=filter_content['pattern']
        )


def get_filter(string: str, filter_type: Type["StringFilter | FormFilter"]) -> "StringFilter | FormFilter":

    filter_content = get_filter_content(string)
    filter = filter_type()

    if not (filter_content.get('is_valid') or filter_content.get('strings') or filter_content.get('pattern')):
        return filter       # return None ?
    
    if type(filter) == StringFilter:
        filter.strings = filter_content['strings']
    elif type(filter) == FormFilter:
        filter.forms = [get_record_id_from_string(string) for string in filter_content['strings']]
    
    filter.pattern = filter_content['pattern']

    return filter

def get_filters(string: str, filter_type: Type["StringFilter | FormFilter"]) -> "StringFilters | FormFilters":

    # filters_content = [get_filter(filter, filter_type) for filter in string.split(",")]

    # filters_content = []
    filters = FormFilters() if filter_type == FormFilter else StringFilters()

    for item in string.split(","):
        filter = get_filter(item, filter_type)
        if filter == filter_type(): continue
        filter.index = len(filters.content) + 1

        if type(filters) == StringFilters and type(filter) == StringFilter:
            filters.content.append(filter)
        elif type(filters) == FormFilters and type(filter) == FormFilter:
            filters.content.append(filter)

    return filters
    # filters_content = get_filters_content(string)
    # filters_list = string.split(",")
    # filters_content = []

    # for filter in filters_list:
    #     filter_content = get_filter_content(filter, patterns)

    #     if filter_content.get('is_valid'):
    #         filter_content['index'] = len(filters_content) + 1
    #         filters_content.append(filter_content)

    # filters = filter_type()

# TO REWORK ???
def get_string_filters(string: str) -> StringFilters:

    filters_content = get_filters_content(string)

    string_filters = StringFilters()

    for filter in filters_content:
        if not filter.get('is_valid'): continue
        if not (filter.get('index') or filter.get('strings') or filter.get('pattern')): continue
        string_filter = StringFilter(index=filter['index'],
                                     strings=filter['strings'],
                                     pattern=filter['pattern'])
        string_filters.content.append(string_filter)

    return string_filters


def get_form_filters(string: str) -> FormFilters:

    filters_content = get_filters_content(string)

    form_filters = FormFilters()

    for filter in filters_content:
        if not filter.get('is_valid'): continue
        if not (filter.get('index') or filter.get('strings') or filter.get('pattern')): continue
        form_filter = FormFilter(index=filter['index'],
                                     forms=filter['strings'],
                                     pattern=filter['pattern'])
        form_filters.content.append(form_filter)

    return form_filters


# TO REWORK ???
# rework docstring + comments
def update_string_filter_from_string(input: "str | None" = None) -> "StringFilter | None":

    active_index = app.config['USER_RECORDS'].strings.active
    string_filter_input = input if input else request.form.get('string_filter_input')

    output = StringFilter(index=active_index)

    if string_filter_input:
        if len(string_filter_input) > 0 and string_filter_input[0] in ["-"]:
            output.pattern = string_filter_input[0]
            string_filter_input = string_filter_input[1:]
        
        output.strings = string_filter_input.split("+")

    app.config['USER_RECORDS'].strings.content[active_index - 1] = output
    add_string_to_filter()
    app.config['USER_RECORDS'].strings.content[active_index - 1].strings = clean_strings(
        app.config['USER_RECORDS'].strings.content[active_index - 1].strings
        )

    if len(app.config['USER_RECORDS'].strings.content[active_index - 1].strings) < 1:
        # remove_string_filter()
        remove_active_filter('strings')
        return None

    return app.config['USER_RECORDS'].strings.content[active_index - 1]


# TO REWORK ???
# rework docstring + comments
def update_string_filters_from_string():

    active_index = app.config['USER_RECORDS'].strings.active
    string_filters_input = request.form.get('string_filters_input')

    output = StringFilters()
    empty_filters_index_list = []

    if string_filters_input:
        filters_list = string_filters_input.split(",")
        output = StringFilters(
            content=[StringFilter(index=i) for i in range(1, len(filters_list) + 1)]
            )
        app.config['USER_RECORDS'].strings = output

        for filter in filters_list:
            app.config['USER_RECORDS'].strings.active = filters_list.index(filter) + 1
            # app.config['USER_RECORDS'].strings.active += 1
            active_index = app.config['USER_RECORDS'].strings.active
            string_filter = update_string_filter_from_string(filter)

            # if not string_filter: continue
            # output.content.append(string_filter)

            # if string_filter: output.content.append(string_filter)
            if string_filter: output.content[active_index - 1] = string_filter
            # else : output.content.append(StringFilter(index=-1))
            else :
                output.content.append(StringFilter(index=active_index))
                # output.content[active_index - 1] = StringFilter(index=active_index)
                empty_filters_index_list.append(active_index)

    # output.strings = string_filter_input.split("+")

    app.config['USER_RECORDS'].strings = output

    for string_filter in output.content:
        if string_filter.index in empty_filters_index_list:
            app.config['USER_RECORDS'].strings.active = string_filter.index
            # remove_string_filter()
            remove_active_filter('strings')

    print(app.config['USER_RECORDS'].strings)



def get_record_id_from_string(input: str) -> RecordId:

    EXTENSIONS = [".esm", ".esp", ".esl"]

    id_type = "FormID" if any(ext in input for ext in EXTENSIONS) and "~" in input else "EditorID"
    record = RecordId(id_type=RecordIdType[id_type])
    
    if id_type == "FormID":
        record_data = input.split("~")

        if len(record_data) == 2:
            try: record.form_id = FormId(record_data[0])
            except: record.form_id = FormId()
            record.mod = record_data[1]
    
    else:
        record_data = get_pattern(input, ["*"])
        if record_data.get('string'): record.editor_id = record_data['string']
        if record_data.get('pattern'): record.pattern = record_data['pattern']
    
    return record
    

    # Decouper le contenu de request.form.get('string_filters_input'),
    # qui correspond à app.config['USER_RECORDS'].strings._get_value()
    # pour modifier les valeurs de app.config['USER_RECORDS'].strings.content
    # dont la valeur vaut par exemple StringFilters(content=[StringFilter(index=1,
    # strings=['testing...'], pattern='', liste=[], negated=False), StringFilter(index=2,
    # strings=['NotMe'], pattern='-', liste=[], negated=False)], active=0)
